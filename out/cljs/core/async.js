// Compiled by ClojureScript 0.0-2234
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.timers');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.ioc_helpers');
cljs.core.async.fn_handler = (function fn_handler(f){if(typeof cljs.core.async.t9440 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9440 = (function (f,fn_handler,meta9441){
this.f = f;
this.fn_handler = fn_handler;
this.meta9441 = meta9441;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9440.cljs$lang$type = true;
cljs.core.async.t9440.cljs$lang$ctorStr = "cljs.core.async/t9440";
cljs.core.async.t9440.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9440");
});
cljs.core.async.t9440.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t9440.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return true;
});
cljs.core.async.t9440.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return self__.f;
});
cljs.core.async.t9440.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9442){var self__ = this;
var _9442__$1 = this;return self__.meta9441;
});
cljs.core.async.t9440.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9442,meta9441__$1){var self__ = this;
var _9442__$1 = this;return (new cljs.core.async.t9440(self__.f,self__.fn_handler,meta9441__$1));
});
cljs.core.async.__GT_t9440 = (function __GT_t9440(f__$1,fn_handler__$1,meta9441){return (new cljs.core.async.t9440(f__$1,fn_handler__$1,meta9441));
});
}
return (new cljs.core.async.t9440(f,fn_handler,null));
});
/**
* Returns a fixed buffer of size n. When full, puts will block/park.
*/
cljs.core.async.buffer = (function buffer(n){return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete but
* val will be dropped (no transfer).
*/
cljs.core.async.dropping_buffer = (function dropping_buffer(n){return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
* Returns a buffer of size n. When full, puts will complete, and be
* buffered, but oldest elements in buffer will be dropped (not
* transferred).
*/
cljs.core.async.sliding_buffer = (function sliding_buffer(n){return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
* Returns true if a channel created with buff will never block. That is to say,
* puts into this buffer will never cause the buffer to be full.
*/
cljs.core.async.unblocking_buffer_QMARK_ = (function unblocking_buffer_QMARK_(buff){var G__9444 = buff;if(G__9444)
{var bit__4188__auto__ = null;if(cljs.core.truth_((function (){var or__3538__auto__ = bit__4188__auto__;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return G__9444.cljs$core$async$impl$protocols$UnblockingBuffer$;
}
})()))
{return true;
} else
{if((!G__9444.cljs$lang$protocol_mask$partition$))
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__9444);
} else
{return false;
}
}
} else
{return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,G__9444);
}
});
/**
* Creates a channel with an optional buffer. If buf-or-n is a number,
* will create and use a fixed buffer of that size.
*/
cljs.core.async.chan = (function() {
var chan = null;
var chan__0 = (function (){return chan.call(null,null);
});
var chan__1 = (function (buf_or_n){var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,0))?null:buf_or_n);return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1));
});
chan = function(buf_or_n){
switch(arguments.length){
case 0:
return chan__0.call(this);
case 1:
return chan__1.call(this,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
chan.cljs$core$IFn$_invoke$arity$0 = chan__0;
chan.cljs$core$IFn$_invoke$arity$1 = chan__1;
return chan;
})()
;
/**
* Returns a channel that will close after msecs
*/
cljs.core.async.timeout = (function timeout(msecs){return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
* takes a val from port. Must be called inside a (go ...) block. Will
* return nil if closed. Will park if nothing is available.
*/
cljs.core.async._LT__BANG_ = (function _LT__BANG_(port){if(null)
{return null;
} else
{throw (new Error(("Assert failed: <! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
});
/**
* Asynchronously takes a val from port, passing to fn1. Will pass nil
* if closed. If on-caller? (default true) is true, and value is
* immediately available, will call fn1 on calling thread.
* Returns nil.
*/
cljs.core.async.take_BANG_ = (function() {
var take_BANG_ = null;
var take_BANG___2 = (function (port,fn1){return take_BANG_.call(null,port,fn1,true);
});
var take_BANG___3 = (function (port,fn1,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));if(cljs.core.truth_(ret))
{var val_9445 = cljs.core.deref.call(null,ret);if(cljs.core.truth_(on_caller_QMARK_))
{fn1.call(null,val_9445);
} else
{cljs.core.async.impl.dispatch.run.call(null,((function (val_9445,ret){
return (function (){return fn1.call(null,val_9445);
});})(val_9445,ret))
);
}
} else
{}
return null;
});
take_BANG_ = function(port,fn1,on_caller_QMARK_){
switch(arguments.length){
case 2:
return take_BANG___2.call(this,port,fn1);
case 3:
return take_BANG___3.call(this,port,fn1,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take_BANG_.cljs$core$IFn$_invoke$arity$2 = take_BANG___2;
take_BANG_.cljs$core$IFn$_invoke$arity$3 = take_BANG___3;
return take_BANG_;
})()
;
cljs.core.async.nop = (function nop(){return null;
});
/**
* puts a val into port. nil values are not allowed. Must be called
* inside a (go ...) block. Will park if no buffer space is available.
*/
cljs.core.async._GT__BANG_ = (function _GT__BANG_(port,val){if(null)
{return null;
} else
{throw (new Error(("Assert failed: >! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
});
/**
* Asynchronously puts a val into port, calling fn0 (if supplied) when
* complete. nil values are not allowed. Will throw if closed. If
* on-caller? (default true) is true, and the put is immediately
* accepted, will call fn0 on calling thread.  Returns nil.
*/
cljs.core.async.put_BANG_ = (function() {
var put_BANG_ = null;
var put_BANG___2 = (function (port,val){return put_BANG_.call(null,port,val,cljs.core.async.nop);
});
var put_BANG___3 = (function (port,val,fn0){return put_BANG_.call(null,port,val,fn0,true);
});
var put_BANG___4 = (function (port,val,fn0,on_caller_QMARK_){var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn0));if(cljs.core.truth_((function (){var and__3526__auto__ = ret;if(cljs.core.truth_(and__3526__auto__))
{return cljs.core.not_EQ_.call(null,fn0,cljs.core.async.nop);
} else
{return and__3526__auto__;
}
})()))
{if(cljs.core.truth_(on_caller_QMARK_))
{fn0.call(null);
} else
{cljs.core.async.impl.dispatch.run.call(null,fn0);
}
} else
{}
return null;
});
put_BANG_ = function(port,val,fn0,on_caller_QMARK_){
switch(arguments.length){
case 2:
return put_BANG___2.call(this,port,val);
case 3:
return put_BANG___3.call(this,port,val,fn0);
case 4:
return put_BANG___4.call(this,port,val,fn0,on_caller_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
put_BANG_.cljs$core$IFn$_invoke$arity$2 = put_BANG___2;
put_BANG_.cljs$core$IFn$_invoke$arity$3 = put_BANG___3;
put_BANG_.cljs$core$IFn$_invoke$arity$4 = put_BANG___4;
return put_BANG_;
})()
;
cljs.core.async.close_BANG_ = (function close_BANG_(port){return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function random_array(n){var a = (new Array(n));var n__4394__auto___9446 = n;var x_9447 = 0;while(true){
if((x_9447 < n__4394__auto___9446))
{(a[x_9447] = 0);
{
var G__9448 = (x_9447 + 1);
x_9447 = G__9448;
continue;
}
} else
{}
break;
}
var i = 1;while(true){
if(cljs.core._EQ_.call(null,i,n))
{return a;
} else
{var j = cljs.core.rand_int.call(null,i);(a[i] = (a[j]));
(a[j] = i);
{
var G__9449 = (i + 1);
i = G__9449;
continue;
}
}
break;
}
});
cljs.core.async.alt_flag = (function alt_flag(){var flag = cljs.core.atom.call(null,true);if(typeof cljs.core.async.t9453 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9453 = (function (flag,alt_flag,meta9454){
this.flag = flag;
this.alt_flag = alt_flag;
this.meta9454 = meta9454;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9453.cljs$lang$type = true;
cljs.core.async.t9453.cljs$lang$ctorStr = "cljs.core.async/t9453";
cljs.core.async.t9453.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9453");
});})(flag))
;
cljs.core.async.t9453.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t9453.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){var self__ = this;
var ___$1 = this;return cljs.core.deref.call(null,self__.flag);
});})(flag))
;
cljs.core.async.t9453.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.flag,null);
return true;
});})(flag))
;
cljs.core.async.t9453.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_9455){var self__ = this;
var _9455__$1 = this;return self__.meta9454;
});})(flag))
;
cljs.core.async.t9453.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_9455,meta9454__$1){var self__ = this;
var _9455__$1 = this;return (new cljs.core.async.t9453(self__.flag,self__.alt_flag,meta9454__$1));
});})(flag))
;
cljs.core.async.__GT_t9453 = ((function (flag){
return (function __GT_t9453(flag__$1,alt_flag__$1,meta9454){return (new cljs.core.async.t9453(flag__$1,alt_flag__$1,meta9454));
});})(flag))
;
}
return (new cljs.core.async.t9453(flag,alt_flag,null));
});
cljs.core.async.alt_handler = (function alt_handler(flag,cb){if(typeof cljs.core.async.t9459 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9459 = (function (cb,flag,alt_handler,meta9460){
this.cb = cb;
this.flag = flag;
this.alt_handler = alt_handler;
this.meta9460 = meta9460;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9459.cljs$lang$type = true;
cljs.core.async.t9459.cljs$lang$ctorStr = "cljs.core.async/t9459";
cljs.core.async.t9459.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9459");
});
cljs.core.async.t9459.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t9459.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});
cljs.core.async.t9459.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){var self__ = this;
var ___$1 = this;cljs.core.async.impl.protocols.commit.call(null,self__.flag);
return self__.cb;
});
cljs.core.async.t9459.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9461){var self__ = this;
var _9461__$1 = this;return self__.meta9460;
});
cljs.core.async.t9459.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9461,meta9460__$1){var self__ = this;
var _9461__$1 = this;return (new cljs.core.async.t9459(self__.cb,self__.flag,self__.alt_handler,meta9460__$1));
});
cljs.core.async.__GT_t9459 = (function __GT_t9459(cb__$1,flag__$1,alt_handler__$1,meta9460){return (new cljs.core.async.t9459(cb__$1,flag__$1,alt_handler__$1,meta9460));
});
}
return (new cljs.core.async.t9459(cb,flag,alt_handler,null));
});
/**
* returns derefable [val port] if immediate, nil if enqueued
*/
cljs.core.async.do_alts = (function do_alts(fret,ports,opts){var flag = cljs.core.async.alt_flag.call(null);var n = cljs.core.count.call(null,ports);var idxs = cljs.core.async.random_array.call(null,n);var priority = new cljs.core.Keyword(null,"priority","priority",4143410454).cljs$core$IFn$_invoke$arity$1(opts);var ret = (function (){var i = 0;while(true){
if((i < n))
{var idx = (cljs.core.truth_(priority)?i:(idxs[i]));var port = cljs.core.nth.call(null,ports,idx);var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,0):null);var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,1);return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (){return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__9462_SHARP_){return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__9462_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));if(cljs.core.truth_(vbox))
{return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__3538__auto__ = wport;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return port;
}
})()], null));
} else
{{
var G__9463 = (i + 1);
i = G__9463;
continue;
}
}
} else
{return null;
}
break;
}
})();var or__3538__auto__ = ret;if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",2558708147)))
{var temp__4092__auto__ = (function (){var and__3526__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);if(cljs.core.truth_(and__3526__auto__))
{return cljs.core.async.impl.protocols.commit.call(null,flag);
} else
{return and__3526__auto__;
}
})();if(cljs.core.truth_(temp__4092__auto__))
{var got = temp__4092__auto__;return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",2558708147).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",2558708147)], null));
} else
{return null;
}
} else
{return null;
}
}
});
/**
* Completes at most one of several channel operations. Must be called
* inside a (go ...) block. ports is a vector of channel endpoints, which
* can be either a channel to take from or a vector of
* [channel-to-put-to val-to-put], in any combination. Takes will be
* made as if by <!, and puts will be made as if by >!. Unless
* the :priority option is true, if more than one port operation is
* ready a non-deterministic choice will be made. If no operation is
* ready and a :default value is supplied, [default-val :default] will
* be returned, otherwise alts! will park until the first operation to
* become ready completes. Returns [val port] of the completed
* operation, where val is the value taken for takes, and nil for puts.
* 
* opts are passed as :key val ... Supported options:
* 
* :default val - the value to use if none of the operations are immediately ready
* :priority true - (default nil) when true, the operations will be tried in order.
* 
* Note: there is no guarantee that the port exps or val exprs will be
* used, nor in what order should they be, so they should not be
* depended upon for side effects.
* @param {...*} var_args
*/
cljs.core.async.alts_BANG_ = (function() { 
var alts_BANG___delegate = function (ports,p__9464){var map__9466 = p__9464;var map__9466__$1 = ((cljs.core.seq_QMARK_.call(null,map__9466))?cljs.core.apply.call(null,cljs.core.hash_map,map__9466):map__9466);var opts = map__9466__$1;if(null)
{return null;
} else
{throw (new Error(("Assert failed: alts! used not in (go ...) block\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,null)))));
}
};
var alts_BANG_ = function (ports,var_args){
var p__9464 = null;if (arguments.length > 1) {
  p__9464 = cljs.core.array_seq(Array.prototype.slice.call(arguments, 1),0);} 
return alts_BANG___delegate.call(this,ports,p__9464);};
alts_BANG_.cljs$lang$maxFixedArity = 1;
alts_BANG_.cljs$lang$applyTo = (function (arglist__9467){
var ports = cljs.core.first(arglist__9467);
var p__9464 = cljs.core.rest(arglist__9467);
return alts_BANG___delegate(ports,p__9464);
});
alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = alts_BANG___delegate;
return alts_BANG_;
})()
;
/**
* Takes a function and a source channel, and returns a channel which
* contains the values produced by applying f to each value taken from
* the source channel
*/
cljs.core.async.map_LT_ = (function map_LT_(f,ch){if(typeof cljs.core.async.t9475 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9475 = (function (ch,f,map_LT_,meta9476){
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta9476 = meta9476;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9475.cljs$lang$type = true;
cljs.core.async.t9475.cljs$lang$ctorStr = "cljs.core.async/t9475";
cljs.core.async.t9475.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9475");
});
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
});
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){if(typeof cljs.core.async.t9478 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9478 = (function (fn1,_,meta9476,ch,f,map_LT_,meta9479){
this.fn1 = fn1;
this._ = _;
this.meta9476 = meta9476;
this.ch = ch;
this.f = f;
this.map_LT_ = map_LT_;
this.meta9479 = meta9479;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9478.cljs$lang$type = true;
cljs.core.async.t9478.cljs$lang$ctorStr = "cljs.core.async/t9478";
cljs.core.async.t9478.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9478");
});})(___$1))
;
cljs.core.async.t9478.prototype.cljs$core$async$impl$protocols$Handler$ = true;
cljs.core.async.t9478.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;
cljs.core.async.t9478.prototype.cljs$core$async$impl$protocols$Handler$lock_id$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;return cljs.core.async.impl.protocols.lock_id.call(null,self__.fn1);
});})(___$1))
;
cljs.core.async.t9478.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$3){var self__ = this;
var ___$4 = this;var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);return ((function (f1,___$4,___$1){
return (function (p1__9468_SHARP_){return f1.call(null,(((p1__9468_SHARP_ == null))?null:self__.f.call(null,p1__9468_SHARP_)));
});
;})(f1,___$4,___$1))
});})(___$1))
;
cljs.core.async.t9478.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_9480){var self__ = this;
var _9480__$1 = this;return self__.meta9479;
});})(___$1))
;
cljs.core.async.t9478.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_9480,meta9479__$1){var self__ = this;
var _9480__$1 = this;return (new cljs.core.async.t9478(self__.fn1,self__._,self__.meta9476,self__.ch,self__.f,self__.map_LT_,meta9479__$1));
});})(___$1))
;
cljs.core.async.__GT_t9478 = ((function (___$1){
return (function __GT_t9478(fn1__$1,___$2,meta9476__$1,ch__$2,f__$2,map_LT___$2,meta9479){return (new cljs.core.async.t9478(fn1__$1,___$2,meta9476__$1,ch__$2,f__$2,map_LT___$2,meta9479));
});})(___$1))
;
}
return (new cljs.core.async.t9478(fn1,___$1,self__.meta9476,self__.ch,self__.f,self__.map_LT_,null));
})());if(cljs.core.truth_((function (){var and__3526__auto__ = ret;if(cljs.core.truth_(and__3526__auto__))
{return !((cljs.core.deref.call(null,ret) == null));
} else
{return and__3526__auto__;
}
})()))
{return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else
{return ret;
}
});
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t9475.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t9475.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9477){var self__ = this;
var _9477__$1 = this;return self__.meta9476;
});
cljs.core.async.t9475.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9477,meta9476__$1){var self__ = this;
var _9477__$1 = this;return (new cljs.core.async.t9475(self__.ch,self__.f,self__.map_LT_,meta9476__$1));
});
cljs.core.async.__GT_t9475 = (function __GT_t9475(ch__$1,f__$1,map_LT___$1,meta9476){return (new cljs.core.async.t9475(ch__$1,f__$1,map_LT___$1,meta9476));
});
}
return (new cljs.core.async.t9475(ch,f,map_LT_,null));
});
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value before supplying it to the target channel.
*/
cljs.core.async.map_GT_ = (function map_GT_(f,ch){if(typeof cljs.core.async.t9484 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9484 = (function (ch,f,map_GT_,meta9485){
this.ch = ch;
this.f = f;
this.map_GT_ = map_GT_;
this.meta9485 = meta9485;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9484.cljs$lang$type = true;
cljs.core.async.t9484.cljs$lang$ctorStr = "cljs.core.async/t9484";
cljs.core.async.t9484.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9484");
});
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn0);
});
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t9484.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t9484.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9486){var self__ = this;
var _9486__$1 = this;return self__.meta9485;
});
cljs.core.async.t9484.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9486,meta9485__$1){var self__ = this;
var _9486__$1 = this;return (new cljs.core.async.t9484(self__.ch,self__.f,self__.map_GT_,meta9485__$1));
});
cljs.core.async.__GT_t9484 = (function __GT_t9484(ch__$1,f__$1,map_GT___$1,meta9485){return (new cljs.core.async.t9484(ch__$1,f__$1,map_GT___$1,meta9485));
});
}
return (new cljs.core.async.t9484(ch,f,map_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns true to the
* target channel.
*/
cljs.core.async.filter_GT_ = (function filter_GT_(p,ch){if(typeof cljs.core.async.t9490 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t9490 = (function (ch,p,filter_GT_,meta9491){
this.ch = ch;
this.p = p;
this.filter_GT_ = filter_GT_;
this.meta9491 = meta9491;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t9490.cljs$lang$type = true;
cljs.core.async.t9490.cljs$lang$ctorStr = "cljs.core.async/t9490";
cljs.core.async.t9490.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t9490");
});
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$WritePort$ = true;
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn0){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.p.call(null,val)))
{return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn0);
} else
{return cljs.core.async.impl.channels.box.call(null,null);
}
});
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$Channel$ = true;
cljs.core.async.t9490.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){var self__ = this;
var ___$1 = this;return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});
cljs.core.async.t9490.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9492){var self__ = this;
var _9492__$1 = this;return self__.meta9491;
});
cljs.core.async.t9490.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9492,meta9491__$1){var self__ = this;
var _9492__$1 = this;return (new cljs.core.async.t9490(self__.ch,self__.p,self__.filter_GT_,meta9491__$1));
});
cljs.core.async.__GT_t9490 = (function __GT_t9490(ch__$1,p__$1,filter_GT___$1,meta9491){return (new cljs.core.async.t9490(ch__$1,p__$1,filter_GT___$1,meta9491));
});
}
return (new cljs.core.async.t9490(ch,p,filter_GT_,null));
});
/**
* Takes a predicate and a target channel, and returns a channel which
* supplies only the values for which the predicate returns false to the
* target channel.
*/
cljs.core.async.remove_GT_ = (function remove_GT_(p,ch){return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns true. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.filter_LT_ = (function() {
var filter_LT_ = null;
var filter_LT___2 = (function (p,ch){return filter_LT_.call(null,p,ch,null);
});
var filter_LT___3 = (function (p,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___9575 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___9575,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___9575,out){
return (function (state_9554){var state_val_9555 = (state_9554[1]);if((state_val_9555 === 1))
{var state_9554__$1 = state_9554;var statearr_9556_9576 = state_9554__$1;(statearr_9556_9576[2] = null);
(statearr_9556_9576[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 2))
{var state_9554__$1 = state_9554;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_9554__$1,4,ch);
} else
{if((state_val_9555 === 3))
{var inst_9552 = (state_9554[2]);var state_9554__$1 = state_9554;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_9554__$1,inst_9552);
} else
{if((state_val_9555 === 4))
{var inst_9536 = (state_9554[7]);var inst_9536__$1 = (state_9554[2]);var inst_9537 = (inst_9536__$1 == null);var state_9554__$1 = (function (){var statearr_9557 = state_9554;(statearr_9557[7] = inst_9536__$1);
return statearr_9557;
})();if(cljs.core.truth_(inst_9537))
{var statearr_9558_9577 = state_9554__$1;(statearr_9558_9577[1] = 5);
} else
{var statearr_9559_9578 = state_9554__$1;(statearr_9559_9578[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 5))
{var inst_9539 = cljs.core.async.close_BANG_.call(null,out);var state_9554__$1 = state_9554;var statearr_9560_9579 = state_9554__$1;(statearr_9560_9579[2] = inst_9539);
(statearr_9560_9579[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 6))
{var inst_9536 = (state_9554[7]);var inst_9541 = p.call(null,inst_9536);var state_9554__$1 = state_9554;if(cljs.core.truth_(inst_9541))
{var statearr_9561_9580 = state_9554__$1;(statearr_9561_9580[1] = 8);
} else
{var statearr_9562_9581 = state_9554__$1;(statearr_9562_9581[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 7))
{var inst_9550 = (state_9554[2]);var state_9554__$1 = state_9554;var statearr_9563_9582 = state_9554__$1;(statearr_9563_9582[2] = inst_9550);
(statearr_9563_9582[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 8))
{var inst_9536 = (state_9554[7]);var state_9554__$1 = state_9554;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_9554__$1,11,out,inst_9536);
} else
{if((state_val_9555 === 9))
{var state_9554__$1 = state_9554;var statearr_9564_9583 = state_9554__$1;(statearr_9564_9583[2] = null);
(statearr_9564_9583[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 10))
{var inst_9547 = (state_9554[2]);var state_9554__$1 = (function (){var statearr_9565 = state_9554;(statearr_9565[8] = inst_9547);
return statearr_9565;
})();var statearr_9566_9584 = state_9554__$1;(statearr_9566_9584[2] = null);
(statearr_9566_9584[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9555 === 11))
{var inst_9544 = (state_9554[2]);var state_9554__$1 = state_9554;var statearr_9567_9585 = state_9554__$1;(statearr_9567_9585[2] = inst_9544);
(statearr_9567_9585[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___9575,out))
;return ((function (switch__7040__auto__,c__7111__auto___9575,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_9571 = [null,null,null,null,null,null,null,null,null];(statearr_9571[0] = state_machine__7041__auto__);
(statearr_9571[1] = 1);
return statearr_9571;
});
var state_machine__7041__auto____1 = (function (state_9554){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_9554);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e9572){if((e9572 instanceof Object))
{var ex__7044__auto__ = e9572;var statearr_9573_9586 = state_9554;(statearr_9573_9586[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_9554);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e9572;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__9587 = state_9554;
state_9554 = G__9587;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_9554){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_9554);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___9575,out))
})();var state__7113__auto__ = (function (){var statearr_9574 = f__7112__auto__.call(null);(statearr_9574[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___9575);
return statearr_9574;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___9575,out))
);
return out;
});
filter_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return filter_LT___2.call(this,p,ch);
case 3:
return filter_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
filter_LT_.cljs$core$IFn$_invoke$arity$2 = filter_LT___2;
filter_LT_.cljs$core$IFn$_invoke$arity$3 = filter_LT___3;
return filter_LT_;
})()
;
/**
* Takes a predicate and a source channel, and returns a channel which
* contains only the values taken from the source channel for which the
* predicate returns false. The returned channel will be unbuffered by
* default, or a buf-or-n can be supplied. The channel will close
* when the source channel closes.
*/
cljs.core.async.remove_LT_ = (function() {
var remove_LT_ = null;
var remove_LT___2 = (function (p,ch){return remove_LT_.call(null,p,ch,null);
});
var remove_LT___3 = (function (p,ch,buf_or_n){return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});
remove_LT_ = function(p,ch,buf_or_n){
switch(arguments.length){
case 2:
return remove_LT___2.call(this,p,ch);
case 3:
return remove_LT___3.call(this,p,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
remove_LT_.cljs$core$IFn$_invoke$arity$2 = remove_LT___2;
remove_LT_.cljs$core$IFn$_invoke$arity$3 = remove_LT___3;
return remove_LT_;
})()
;
cljs.core.async.mapcat_STAR_ = (function mapcat_STAR_(f,in$,out){var c__7111__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto__){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto__){
return (function (state_9739){var state_val_9740 = (state_9739[1]);if((state_val_9740 === 1))
{var state_9739__$1 = state_9739;var statearr_9741_9778 = state_9739__$1;(statearr_9741_9778[2] = null);
(statearr_9741_9778[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 2))
{var state_9739__$1 = state_9739;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_9739__$1,4,in$);
} else
{if((state_val_9740 === 3))
{var inst_9737 = (state_9739[2]);var state_9739__$1 = state_9739;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_9739__$1,inst_9737);
} else
{if((state_val_9740 === 4))
{var inst_9685 = (state_9739[7]);var inst_9685__$1 = (state_9739[2]);var inst_9686 = (inst_9685__$1 == null);var state_9739__$1 = (function (){var statearr_9742 = state_9739;(statearr_9742[7] = inst_9685__$1);
return statearr_9742;
})();if(cljs.core.truth_(inst_9686))
{var statearr_9743_9779 = state_9739__$1;(statearr_9743_9779[1] = 5);
} else
{var statearr_9744_9780 = state_9739__$1;(statearr_9744_9780[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 5))
{var inst_9688 = cljs.core.async.close_BANG_.call(null,out);var state_9739__$1 = state_9739;var statearr_9745_9781 = state_9739__$1;(statearr_9745_9781[2] = inst_9688);
(statearr_9745_9781[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 6))
{var inst_9685 = (state_9739[7]);var inst_9690 = f.call(null,inst_9685);var inst_9695 = cljs.core.seq.call(null,inst_9690);var inst_9696 = inst_9695;var inst_9697 = null;var inst_9698 = 0;var inst_9699 = 0;var state_9739__$1 = (function (){var statearr_9746 = state_9739;(statearr_9746[8] = inst_9699);
(statearr_9746[9] = inst_9697);
(statearr_9746[10] = inst_9698);
(statearr_9746[11] = inst_9696);
return statearr_9746;
})();var statearr_9747_9782 = state_9739__$1;(statearr_9747_9782[2] = null);
(statearr_9747_9782[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 7))
{var inst_9735 = (state_9739[2]);var state_9739__$1 = state_9739;var statearr_9748_9783 = state_9739__$1;(statearr_9748_9783[2] = inst_9735);
(statearr_9748_9783[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 8))
{var inst_9699 = (state_9739[8]);var inst_9698 = (state_9739[10]);var inst_9701 = (inst_9699 < inst_9698);var inst_9702 = inst_9701;var state_9739__$1 = state_9739;if(cljs.core.truth_(inst_9702))
{var statearr_9749_9784 = state_9739__$1;(statearr_9749_9784[1] = 10);
} else
{var statearr_9750_9785 = state_9739__$1;(statearr_9750_9785[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 9))
{var inst_9732 = (state_9739[2]);var state_9739__$1 = (function (){var statearr_9751 = state_9739;(statearr_9751[12] = inst_9732);
return statearr_9751;
})();var statearr_9752_9786 = state_9739__$1;(statearr_9752_9786[2] = null);
(statearr_9752_9786[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 10))
{var inst_9699 = (state_9739[8]);var inst_9697 = (state_9739[9]);var inst_9704 = cljs.core._nth.call(null,inst_9697,inst_9699);var state_9739__$1 = state_9739;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_9739__$1,13,out,inst_9704);
} else
{if((state_val_9740 === 11))
{var inst_9710 = (state_9739[13]);var inst_9696 = (state_9739[11]);var inst_9710__$1 = cljs.core.seq.call(null,inst_9696);var state_9739__$1 = (function (){var statearr_9756 = state_9739;(statearr_9756[13] = inst_9710__$1);
return statearr_9756;
})();if(inst_9710__$1)
{var statearr_9757_9787 = state_9739__$1;(statearr_9757_9787[1] = 14);
} else
{var statearr_9758_9788 = state_9739__$1;(statearr_9758_9788[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 12))
{var inst_9730 = (state_9739[2]);var state_9739__$1 = state_9739;var statearr_9759_9789 = state_9739__$1;(statearr_9759_9789[2] = inst_9730);
(statearr_9759_9789[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 13))
{var inst_9699 = (state_9739[8]);var inst_9697 = (state_9739[9]);var inst_9698 = (state_9739[10]);var inst_9696 = (state_9739[11]);var inst_9706 = (state_9739[2]);var inst_9707 = (inst_9699 + 1);var tmp9753 = inst_9697;var tmp9754 = inst_9698;var tmp9755 = inst_9696;var inst_9696__$1 = tmp9755;var inst_9697__$1 = tmp9753;var inst_9698__$1 = tmp9754;var inst_9699__$1 = inst_9707;var state_9739__$1 = (function (){var statearr_9760 = state_9739;(statearr_9760[14] = inst_9706);
(statearr_9760[8] = inst_9699__$1);
(statearr_9760[9] = inst_9697__$1);
(statearr_9760[10] = inst_9698__$1);
(statearr_9760[11] = inst_9696__$1);
return statearr_9760;
})();var statearr_9761_9790 = state_9739__$1;(statearr_9761_9790[2] = null);
(statearr_9761_9790[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 14))
{var inst_9710 = (state_9739[13]);var inst_9712 = cljs.core.chunked_seq_QMARK_.call(null,inst_9710);var state_9739__$1 = state_9739;if(inst_9712)
{var statearr_9762_9791 = state_9739__$1;(statearr_9762_9791[1] = 17);
} else
{var statearr_9763_9792 = state_9739__$1;(statearr_9763_9792[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 15))
{var state_9739__$1 = state_9739;var statearr_9764_9793 = state_9739__$1;(statearr_9764_9793[2] = null);
(statearr_9764_9793[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 16))
{var inst_9728 = (state_9739[2]);var state_9739__$1 = state_9739;var statearr_9765_9794 = state_9739__$1;(statearr_9765_9794[2] = inst_9728);
(statearr_9765_9794[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 17))
{var inst_9710 = (state_9739[13]);var inst_9714 = cljs.core.chunk_first.call(null,inst_9710);var inst_9715 = cljs.core.chunk_rest.call(null,inst_9710);var inst_9716 = cljs.core.count.call(null,inst_9714);var inst_9696 = inst_9715;var inst_9697 = inst_9714;var inst_9698 = inst_9716;var inst_9699 = 0;var state_9739__$1 = (function (){var statearr_9766 = state_9739;(statearr_9766[8] = inst_9699);
(statearr_9766[9] = inst_9697);
(statearr_9766[10] = inst_9698);
(statearr_9766[11] = inst_9696);
return statearr_9766;
})();var statearr_9767_9795 = state_9739__$1;(statearr_9767_9795[2] = null);
(statearr_9767_9795[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 18))
{var inst_9710 = (state_9739[13]);var inst_9719 = cljs.core.first.call(null,inst_9710);var state_9739__$1 = state_9739;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_9739__$1,20,out,inst_9719);
} else
{if((state_val_9740 === 19))
{var inst_9725 = (state_9739[2]);var state_9739__$1 = state_9739;var statearr_9768_9796 = state_9739__$1;(statearr_9768_9796[2] = inst_9725);
(statearr_9768_9796[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9740 === 20))
{var inst_9710 = (state_9739[13]);var inst_9721 = (state_9739[2]);var inst_9722 = cljs.core.next.call(null,inst_9710);var inst_9696 = inst_9722;var inst_9697 = null;var inst_9698 = 0;var inst_9699 = 0;var state_9739__$1 = (function (){var statearr_9769 = state_9739;(statearr_9769[15] = inst_9721);
(statearr_9769[8] = inst_9699);
(statearr_9769[9] = inst_9697);
(statearr_9769[10] = inst_9698);
(statearr_9769[11] = inst_9696);
return statearr_9769;
})();var statearr_9770_9797 = state_9739__$1;(statearr_9770_9797[2] = null);
(statearr_9770_9797[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto__))
;return ((function (switch__7040__auto__,c__7111__auto__){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_9774 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_9774[0] = state_machine__7041__auto__);
(statearr_9774[1] = 1);
return statearr_9774;
});
var state_machine__7041__auto____1 = (function (state_9739){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_9739);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e9775){if((e9775 instanceof Object))
{var ex__7044__auto__ = e9775;var statearr_9776_9798 = state_9739;(statearr_9776_9798[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_9739);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e9775;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__9799 = state_9739;
state_9739 = G__9799;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_9739){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_9739);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto__))
})();var state__7113__auto__ = (function (){var statearr_9777 = f__7112__auto__.call(null);(statearr_9777[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto__);
return statearr_9777;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto__))
);
return c__7111__auto__;
});
/**
* Takes a function and a source channel, and returns a channel which
* contains the values in each collection produced by applying f to
* each value taken from the source channel. f must return a
* collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The channel will close when the source channel
* closes.
*/
cljs.core.async.mapcat_LT_ = (function() {
var mapcat_LT_ = null;
var mapcat_LT___2 = (function (f,in$){return mapcat_LT_.call(null,f,in$,null);
});
var mapcat_LT___3 = (function (f,in$,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return out;
});
mapcat_LT_ = function(f,in$,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_LT___2.call(this,f,in$);
case 3:
return mapcat_LT___3.call(this,f,in$,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = mapcat_LT___2;
mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = mapcat_LT___3;
return mapcat_LT_;
})()
;
/**
* Takes a function and a target channel, and returns a channel which
* applies f to each value put, then supplies each element of the result
* to the target channel. f must return a collection.
* 
* The returned channel will be unbuffered by default, or a buf-or-n
* can be supplied. The target channel will be closed when the source
* channel closes.
*/
cljs.core.async.mapcat_GT_ = (function() {
var mapcat_GT_ = null;
var mapcat_GT___2 = (function (f,out){return mapcat_GT_.call(null,f,out,null);
});
var mapcat_GT___3 = (function (f,out,buf_or_n){var in$ = cljs.core.async.chan.call(null,buf_or_n);cljs.core.async.mapcat_STAR_.call(null,f,in$,out);
return in$;
});
mapcat_GT_ = function(f,out,buf_or_n){
switch(arguments.length){
case 2:
return mapcat_GT___2.call(this,f,out);
case 3:
return mapcat_GT___3.call(this,f,out,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = mapcat_GT___2;
mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = mapcat_GT___3;
return mapcat_GT_;
})()
;
/**
* Takes elements from the from channel and supplies them to the to
* channel. By default, the to channel will be closed when the
* from channel closes, but can be determined by the close?
* parameter.
*/
cljs.core.async.pipe = (function() {
var pipe = null;
var pipe__2 = (function (from,to){return pipe.call(null,from,to,true);
});
var pipe__3 = (function (from,to,close_QMARK_){var c__7111__auto___9880 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___9880){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___9880){
return (function (state_9859){var state_val_9860 = (state_9859[1]);if((state_val_9860 === 1))
{var state_9859__$1 = state_9859;var statearr_9861_9881 = state_9859__$1;(statearr_9861_9881[2] = null);
(statearr_9861_9881[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 2))
{var state_9859__$1 = state_9859;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_9859__$1,4,from);
} else
{if((state_val_9860 === 3))
{var inst_9857 = (state_9859[2]);var state_9859__$1 = state_9859;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_9859__$1,inst_9857);
} else
{if((state_val_9860 === 4))
{var inst_9842 = (state_9859[7]);var inst_9842__$1 = (state_9859[2]);var inst_9843 = (inst_9842__$1 == null);var state_9859__$1 = (function (){var statearr_9862 = state_9859;(statearr_9862[7] = inst_9842__$1);
return statearr_9862;
})();if(cljs.core.truth_(inst_9843))
{var statearr_9863_9882 = state_9859__$1;(statearr_9863_9882[1] = 5);
} else
{var statearr_9864_9883 = state_9859__$1;(statearr_9864_9883[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 5))
{var state_9859__$1 = state_9859;if(cljs.core.truth_(close_QMARK_))
{var statearr_9865_9884 = state_9859__$1;(statearr_9865_9884[1] = 8);
} else
{var statearr_9866_9885 = state_9859__$1;(statearr_9866_9885[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 6))
{var inst_9842 = (state_9859[7]);var state_9859__$1 = state_9859;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_9859__$1,11,to,inst_9842);
} else
{if((state_val_9860 === 7))
{var inst_9855 = (state_9859[2]);var state_9859__$1 = state_9859;var statearr_9867_9886 = state_9859__$1;(statearr_9867_9886[2] = inst_9855);
(statearr_9867_9886[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 8))
{var inst_9846 = cljs.core.async.close_BANG_.call(null,to);var state_9859__$1 = state_9859;var statearr_9868_9887 = state_9859__$1;(statearr_9868_9887[2] = inst_9846);
(statearr_9868_9887[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 9))
{var state_9859__$1 = state_9859;var statearr_9869_9888 = state_9859__$1;(statearr_9869_9888[2] = null);
(statearr_9869_9888[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 10))
{var inst_9849 = (state_9859[2]);var state_9859__$1 = state_9859;var statearr_9870_9889 = state_9859__$1;(statearr_9870_9889[2] = inst_9849);
(statearr_9870_9889[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9860 === 11))
{var inst_9852 = (state_9859[2]);var state_9859__$1 = (function (){var statearr_9871 = state_9859;(statearr_9871[8] = inst_9852);
return statearr_9871;
})();var statearr_9872_9890 = state_9859__$1;(statearr_9872_9890[2] = null);
(statearr_9872_9890[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___9880))
;return ((function (switch__7040__auto__,c__7111__auto___9880){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_9876 = [null,null,null,null,null,null,null,null,null];(statearr_9876[0] = state_machine__7041__auto__);
(statearr_9876[1] = 1);
return statearr_9876;
});
var state_machine__7041__auto____1 = (function (state_9859){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_9859);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e9877){if((e9877 instanceof Object))
{var ex__7044__auto__ = e9877;var statearr_9878_9891 = state_9859;(statearr_9878_9891[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_9859);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e9877;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__9892 = state_9859;
state_9859 = G__9892;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_9859){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_9859);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___9880))
})();var state__7113__auto__ = (function (){var statearr_9879 = f__7112__auto__.call(null);(statearr_9879[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___9880);
return statearr_9879;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___9880))
);
return to;
});
pipe = function(from,to,close_QMARK_){
switch(arguments.length){
case 2:
return pipe__2.call(this,from,to);
case 3:
return pipe__3.call(this,from,to,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pipe.cljs$core$IFn$_invoke$arity$2 = pipe__2;
pipe.cljs$core$IFn$_invoke$arity$3 = pipe__3;
return pipe;
})()
;
/**
* Takes a predicate and a source channel and returns a vector of two
* channels, the first of which will contain the values for which the
* predicate returned true, the second those for which it returned
* false.
* 
* The out channels will be unbuffered by default, or two buf-or-ns can
* be supplied. The channels will close after the source channel has
* closed.
*/
cljs.core.async.split = (function() {
var split = null;
var split__2 = (function (p,ch){return split.call(null,p,ch,null,null);
});
var split__4 = (function (p,ch,t_buf_or_n,f_buf_or_n){var tc = cljs.core.async.chan.call(null,t_buf_or_n);var fc = cljs.core.async.chan.call(null,f_buf_or_n);var c__7111__auto___9979 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___9979,tc,fc){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___9979,tc,fc){
return (function (state_9957){var state_val_9958 = (state_9957[1]);if((state_val_9958 === 1))
{var state_9957__$1 = state_9957;var statearr_9959_9980 = state_9957__$1;(statearr_9959_9980[2] = null);
(statearr_9959_9980[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 2))
{var state_9957__$1 = state_9957;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_9957__$1,4,ch);
} else
{if((state_val_9958 === 3))
{var inst_9955 = (state_9957[2]);var state_9957__$1 = state_9957;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_9957__$1,inst_9955);
} else
{if((state_val_9958 === 4))
{var inst_9938 = (state_9957[7]);var inst_9938__$1 = (state_9957[2]);var inst_9939 = (inst_9938__$1 == null);var state_9957__$1 = (function (){var statearr_9960 = state_9957;(statearr_9960[7] = inst_9938__$1);
return statearr_9960;
})();if(cljs.core.truth_(inst_9939))
{var statearr_9961_9981 = state_9957__$1;(statearr_9961_9981[1] = 5);
} else
{var statearr_9962_9982 = state_9957__$1;(statearr_9962_9982[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 5))
{var inst_9941 = cljs.core.async.close_BANG_.call(null,tc);var inst_9942 = cljs.core.async.close_BANG_.call(null,fc);var state_9957__$1 = (function (){var statearr_9963 = state_9957;(statearr_9963[8] = inst_9941);
return statearr_9963;
})();var statearr_9964_9983 = state_9957__$1;(statearr_9964_9983[2] = inst_9942);
(statearr_9964_9983[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 6))
{var inst_9938 = (state_9957[7]);var inst_9944 = p.call(null,inst_9938);var state_9957__$1 = state_9957;if(cljs.core.truth_(inst_9944))
{var statearr_9965_9984 = state_9957__$1;(statearr_9965_9984[1] = 9);
} else
{var statearr_9966_9985 = state_9957__$1;(statearr_9966_9985[1] = 10);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 7))
{var inst_9953 = (state_9957[2]);var state_9957__$1 = state_9957;var statearr_9967_9986 = state_9957__$1;(statearr_9967_9986[2] = inst_9953);
(statearr_9967_9986[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 8))
{var inst_9950 = (state_9957[2]);var state_9957__$1 = (function (){var statearr_9968 = state_9957;(statearr_9968[9] = inst_9950);
return statearr_9968;
})();var statearr_9969_9987 = state_9957__$1;(statearr_9969_9987[2] = null);
(statearr_9969_9987[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 9))
{var state_9957__$1 = state_9957;var statearr_9970_9988 = state_9957__$1;(statearr_9970_9988[2] = tc);
(statearr_9970_9988[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 10))
{var state_9957__$1 = state_9957;var statearr_9971_9989 = state_9957__$1;(statearr_9971_9989[2] = fc);
(statearr_9971_9989[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9958 === 11))
{var inst_9938 = (state_9957[7]);var inst_9948 = (state_9957[2]);var state_9957__$1 = state_9957;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_9957__$1,8,inst_9948,inst_9938);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___9979,tc,fc))
;return ((function (switch__7040__auto__,c__7111__auto___9979,tc,fc){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_9975 = [null,null,null,null,null,null,null,null,null,null];(statearr_9975[0] = state_machine__7041__auto__);
(statearr_9975[1] = 1);
return statearr_9975;
});
var state_machine__7041__auto____1 = (function (state_9957){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_9957);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e9976){if((e9976 instanceof Object))
{var ex__7044__auto__ = e9976;var statearr_9977_9990 = state_9957;(statearr_9977_9990[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_9957);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e9976;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__9991 = state_9957;
state_9957 = G__9991;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_9957){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_9957);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___9979,tc,fc))
})();var state__7113__auto__ = (function (){var statearr_9978 = f__7112__auto__.call(null);(statearr_9978[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___9979);
return statearr_9978;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___9979,tc,fc))
);
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});
split = function(p,ch,t_buf_or_n,f_buf_or_n){
switch(arguments.length){
case 2:
return split__2.call(this,p,ch);
case 4:
return split__4.call(this,p,ch,t_buf_or_n,f_buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
split.cljs$core$IFn$_invoke$arity$2 = split__2;
split.cljs$core$IFn$_invoke$arity$4 = split__4;
return split;
})()
;
/**
* f should be a function of 2 arguments. Returns a channel containing
* the single result of applying f to init and the first item from the
* channel, then applying f to that result and the 2nd item, etc. If
* the channel closes without yielding items, returns init and f is not
* called. ch must close before reduce produces a result.
*/
cljs.core.async.reduce = (function reduce(f,init,ch){var c__7111__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto__){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto__){
return (function (state_10038){var state_val_10039 = (state_10038[1]);if((state_val_10039 === 7))
{var inst_10034 = (state_10038[2]);var state_10038__$1 = state_10038;var statearr_10040_10056 = state_10038__$1;(statearr_10040_10056[2] = inst_10034);
(statearr_10040_10056[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10039 === 6))
{var inst_10027 = (state_10038[7]);var inst_10024 = (state_10038[8]);var inst_10031 = f.call(null,inst_10024,inst_10027);var inst_10024__$1 = inst_10031;var state_10038__$1 = (function (){var statearr_10041 = state_10038;(statearr_10041[8] = inst_10024__$1);
return statearr_10041;
})();var statearr_10042_10057 = state_10038__$1;(statearr_10042_10057[2] = null);
(statearr_10042_10057[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10039 === 5))
{var inst_10024 = (state_10038[8]);var state_10038__$1 = state_10038;var statearr_10043_10058 = state_10038__$1;(statearr_10043_10058[2] = inst_10024);
(statearr_10043_10058[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10039 === 4))
{var inst_10027 = (state_10038[7]);var inst_10027__$1 = (state_10038[2]);var inst_10028 = (inst_10027__$1 == null);var state_10038__$1 = (function (){var statearr_10044 = state_10038;(statearr_10044[7] = inst_10027__$1);
return statearr_10044;
})();if(cljs.core.truth_(inst_10028))
{var statearr_10045_10059 = state_10038__$1;(statearr_10045_10059[1] = 5);
} else
{var statearr_10046_10060 = state_10038__$1;(statearr_10046_10060[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10039 === 3))
{var inst_10036 = (state_10038[2]);var state_10038__$1 = state_10038;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_10038__$1,inst_10036);
} else
{if((state_val_10039 === 2))
{var state_10038__$1 = state_10038;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_10038__$1,4,ch);
} else
{if((state_val_10039 === 1))
{var inst_10024 = init;var state_10038__$1 = (function (){var statearr_10047 = state_10038;(statearr_10047[8] = inst_10024);
return statearr_10047;
})();var statearr_10048_10061 = state_10038__$1;(statearr_10048_10061[2] = null);
(statearr_10048_10061[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
});})(c__7111__auto__))
;return ((function (switch__7040__auto__,c__7111__auto__){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_10052 = [null,null,null,null,null,null,null,null,null];(statearr_10052[0] = state_machine__7041__auto__);
(statearr_10052[1] = 1);
return statearr_10052;
});
var state_machine__7041__auto____1 = (function (state_10038){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_10038);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e10053){if((e10053 instanceof Object))
{var ex__7044__auto__ = e10053;var statearr_10054_10062 = state_10038;(statearr_10054_10062[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10038);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e10053;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__10063 = state_10038;
state_10038 = G__10063;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_10038){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_10038);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto__))
})();var state__7113__auto__ = (function (){var statearr_10055 = f__7112__auto__.call(null);(statearr_10055[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto__);
return statearr_10055;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto__))
);
return c__7111__auto__;
});
/**
* Puts the contents of coll into the supplied channel.
* 
* By default the channel will be closed after the items are copied,
* but can be determined by the close? parameter.
* 
* Returns a channel which will close after the items are copied.
*/
cljs.core.async.onto_chan = (function() {
var onto_chan = null;
var onto_chan__2 = (function (ch,coll){return onto_chan.call(null,ch,coll,true);
});
var onto_chan__3 = (function (ch,coll,close_QMARK_){var c__7111__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto__){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto__){
return (function (state_10125){var state_val_10126 = (state_10125[1]);if((state_val_10126 === 1))
{var inst_10105 = cljs.core.seq.call(null,coll);var inst_10106 = inst_10105;var state_10125__$1 = (function (){var statearr_10127 = state_10125;(statearr_10127[7] = inst_10106);
return statearr_10127;
})();var statearr_10128_10146 = state_10125__$1;(statearr_10128_10146[2] = null);
(statearr_10128_10146[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 2))
{var inst_10106 = (state_10125[7]);var state_10125__$1 = state_10125;if(cljs.core.truth_(inst_10106))
{var statearr_10129_10147 = state_10125__$1;(statearr_10129_10147[1] = 4);
} else
{var statearr_10130_10148 = state_10125__$1;(statearr_10130_10148[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 3))
{var inst_10123 = (state_10125[2]);var state_10125__$1 = state_10125;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_10125__$1,inst_10123);
} else
{if((state_val_10126 === 4))
{var inst_10106 = (state_10125[7]);var inst_10109 = cljs.core.first.call(null,inst_10106);var state_10125__$1 = state_10125;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_10125__$1,7,ch,inst_10109);
} else
{if((state_val_10126 === 5))
{var state_10125__$1 = state_10125;if(cljs.core.truth_(close_QMARK_))
{var statearr_10131_10149 = state_10125__$1;(statearr_10131_10149[1] = 8);
} else
{var statearr_10132_10150 = state_10125__$1;(statearr_10132_10150[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 6))
{var inst_10121 = (state_10125[2]);var state_10125__$1 = state_10125;var statearr_10133_10151 = state_10125__$1;(statearr_10133_10151[2] = inst_10121);
(statearr_10133_10151[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 7))
{var inst_10106 = (state_10125[7]);var inst_10111 = (state_10125[2]);var inst_10112 = cljs.core.next.call(null,inst_10106);var inst_10106__$1 = inst_10112;var state_10125__$1 = (function (){var statearr_10134 = state_10125;(statearr_10134[8] = inst_10111);
(statearr_10134[7] = inst_10106__$1);
return statearr_10134;
})();var statearr_10135_10152 = state_10125__$1;(statearr_10135_10152[2] = null);
(statearr_10135_10152[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 8))
{var inst_10116 = cljs.core.async.close_BANG_.call(null,ch);var state_10125__$1 = state_10125;var statearr_10136_10153 = state_10125__$1;(statearr_10136_10153[2] = inst_10116);
(statearr_10136_10153[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 9))
{var state_10125__$1 = state_10125;var statearr_10137_10154 = state_10125__$1;(statearr_10137_10154[2] = null);
(statearr_10137_10154[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10126 === 10))
{var inst_10119 = (state_10125[2]);var state_10125__$1 = state_10125;var statearr_10138_10155 = state_10125__$1;(statearr_10138_10155[2] = inst_10119);
(statearr_10138_10155[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto__))
;return ((function (switch__7040__auto__,c__7111__auto__){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_10142 = [null,null,null,null,null,null,null,null,null];(statearr_10142[0] = state_machine__7041__auto__);
(statearr_10142[1] = 1);
return statearr_10142;
});
var state_machine__7041__auto____1 = (function (state_10125){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_10125);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e10143){if((e10143 instanceof Object))
{var ex__7044__auto__ = e10143;var statearr_10144_10156 = state_10125;(statearr_10144_10156[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10125);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e10143;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__10157 = state_10125;
state_10125 = G__10157;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_10125){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_10125);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto__))
})();var state__7113__auto__ = (function (){var statearr_10145 = f__7112__auto__.call(null);(statearr_10145[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto__);
return statearr_10145;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto__))
);
return c__7111__auto__;
});
onto_chan = function(ch,coll,close_QMARK_){
switch(arguments.length){
case 2:
return onto_chan__2.call(this,ch,coll);
case 3:
return onto_chan__3.call(this,ch,coll,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
onto_chan.cljs$core$IFn$_invoke$arity$2 = onto_chan__2;
onto_chan.cljs$core$IFn$_invoke$arity$3 = onto_chan__3;
return onto_chan;
})()
;
/**
* Creates and returns a channel which contains the contents of coll,
* closing when exhausted.
*/
cljs.core.async.to_chan = (function to_chan(coll){var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,100,coll));cljs.core.async.onto_chan.call(null,ch,coll);
return ch;
});
cljs.core.async.Mux = (function (){var obj10159 = {};return obj10159;
})();
cljs.core.async.muxch_STAR_ = (function muxch_STAR_(_){if((function (){var and__3526__auto__ = _;if(and__3526__auto__)
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else
{var x__4165__auto__ = (((_ == null))?null:_);return (function (){var or__3538__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
})().call(null,_);
}
});
cljs.core.async.Mult = (function (){var obj10161 = {};return obj10161;
})();
cljs.core.async.tap_STAR_ = (function tap_STAR_(m,ch,close_QMARK_){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$tap_STAR_$arity$3;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.tap_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
})().call(null,m,ch,close_QMARK_);
}
});
cljs.core.async.untap_STAR_ = (function untap_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$untap_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.untap_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.untap_all_STAR_ = (function untap_all_STAR_(m){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
})().call(null,m);
}
});
/**
* Creates and returns a mult(iple) of the supplied channel. Channels
* containing copies of the channel can be created with 'tap', and
* detached with 'untap'.
* 
* Each item is distributed to all taps in parallel and synchronously,
* i.e. each tap must accept before the next item is distributed. Use
* buffering/windowing to prevent slow taps from holding up the mult.
* 
* Items received when there are no taps get dropped.
* 
* If a tap put throws an exception, it will be removed from the mult.
*/
cljs.core.async.mult = (function mult(ch){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var m = (function (){if(typeof cljs.core.async.t10385 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10385 = (function (cs,ch,mult,meta10386){
this.cs = cs;
this.ch = ch;
this.mult = mult;
this.meta10386 = meta10386;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10385.cljs$lang$type = true;
cljs.core.async.t10385.cljs$lang$ctorStr = "cljs.core.async/t10385";
cljs.core.async.t10385.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10385");
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$async$Mult$ = true;
cljs.core.async.t10385.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$2,close_QMARK_){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$2,close_QMARK_);
return null;
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$2){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$2);
return null;
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return null;
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t10385.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_10387){var self__ = this;
var _10387__$1 = this;return self__.meta10386;
});})(cs))
;
cljs.core.async.t10385.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_10387,meta10386__$1){var self__ = this;
var _10387__$1 = this;return (new cljs.core.async.t10385(self__.cs,self__.ch,self__.mult,meta10386__$1));
});})(cs))
;
cljs.core.async.__GT_t10385 = ((function (cs){
return (function __GT_t10385(cs__$1,ch__$1,mult__$1,meta10386){return (new cljs.core.async.t10385(cs__$1,ch__$1,mult__$1,meta10386));
});})(cs))
;
}
return (new cljs.core.async.t10385(cs,ch,mult,null));
})();var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = ((function (cs,m,dchan,dctr){
return (function (){if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,true);
} else
{return null;
}
});})(cs,m,dchan,dctr))
;var c__7111__auto___10608 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___10608,cs,m,dchan,dctr,done){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___10608,cs,m,dchan,dctr,done){
return (function (state_10522){var state_val_10523 = (state_10522[1]);if((state_val_10523 === 32))
{var inst_10466 = (state_10522[7]);var inst_10390 = (state_10522[8]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_10522,31,Object,null,30);var inst_10473 = cljs.core.async.put_BANG_.call(null,inst_10466,inst_10390,done);var state_10522__$1 = state_10522;var statearr_10524_10609 = state_10522__$1;(statearr_10524_10609[2] = inst_10473);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10522__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 1))
{var state_10522__$1 = state_10522;var statearr_10525_10610 = state_10522__$1;(statearr_10525_10610[2] = null);
(statearr_10525_10610[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 33))
{var inst_10479 = (state_10522[9]);var inst_10481 = cljs.core.chunked_seq_QMARK_.call(null,inst_10479);var state_10522__$1 = state_10522;if(inst_10481)
{var statearr_10526_10611 = state_10522__$1;(statearr_10526_10611[1] = 36);
} else
{var statearr_10527_10612 = state_10522__$1;(statearr_10527_10612[1] = 37);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 2))
{var state_10522__$1 = state_10522;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_10522__$1,4,ch);
} else
{if((state_val_10523 === 34))
{var state_10522__$1 = state_10522;var statearr_10528_10613 = state_10522__$1;(statearr_10528_10613[2] = null);
(statearr_10528_10613[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 3))
{var inst_10520 = (state_10522[2]);var state_10522__$1 = state_10522;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_10522__$1,inst_10520);
} else
{if((state_val_10523 === 35))
{var inst_10504 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10529_10614 = state_10522__$1;(statearr_10529_10614[2] = inst_10504);
(statearr_10529_10614[1] = 29);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 4))
{var inst_10390 = (state_10522[8]);var inst_10390__$1 = (state_10522[2]);var inst_10391 = (inst_10390__$1 == null);var state_10522__$1 = (function (){var statearr_10530 = state_10522;(statearr_10530[8] = inst_10390__$1);
return statearr_10530;
})();if(cljs.core.truth_(inst_10391))
{var statearr_10531_10615 = state_10522__$1;(statearr_10531_10615[1] = 5);
} else
{var statearr_10532_10616 = state_10522__$1;(statearr_10532_10616[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 36))
{var inst_10479 = (state_10522[9]);var inst_10483 = cljs.core.chunk_first.call(null,inst_10479);var inst_10484 = cljs.core.chunk_rest.call(null,inst_10479);var inst_10485 = cljs.core.count.call(null,inst_10483);var inst_10458 = inst_10484;var inst_10459 = inst_10483;var inst_10460 = inst_10485;var inst_10461 = 0;var state_10522__$1 = (function (){var statearr_10533 = state_10522;(statearr_10533[10] = inst_10459);
(statearr_10533[11] = inst_10460);
(statearr_10533[12] = inst_10461);
(statearr_10533[13] = inst_10458);
return statearr_10533;
})();var statearr_10534_10617 = state_10522__$1;(statearr_10534_10617[2] = null);
(statearr_10534_10617[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 5))
{var inst_10397 = cljs.core.deref.call(null,cs);var inst_10398 = cljs.core.seq.call(null,inst_10397);var inst_10399 = inst_10398;var inst_10400 = null;var inst_10401 = 0;var inst_10402 = 0;var state_10522__$1 = (function (){var statearr_10535 = state_10522;(statearr_10535[14] = inst_10400);
(statearr_10535[15] = inst_10401);
(statearr_10535[16] = inst_10402);
(statearr_10535[17] = inst_10399);
return statearr_10535;
})();var statearr_10536_10618 = state_10522__$1;(statearr_10536_10618[2] = null);
(statearr_10536_10618[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 37))
{var inst_10479 = (state_10522[9]);var inst_10488 = cljs.core.first.call(null,inst_10479);var state_10522__$1 = (function (){var statearr_10537 = state_10522;(statearr_10537[18] = inst_10488);
return statearr_10537;
})();var statearr_10538_10619 = state_10522__$1;(statearr_10538_10619[2] = null);
(statearr_10538_10619[1] = 41);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 6))
{var inst_10450 = (state_10522[19]);var inst_10449 = cljs.core.deref.call(null,cs);var inst_10450__$1 = cljs.core.keys.call(null,inst_10449);var inst_10451 = cljs.core.count.call(null,inst_10450__$1);var inst_10452 = cljs.core.reset_BANG_.call(null,dctr,inst_10451);var inst_10457 = cljs.core.seq.call(null,inst_10450__$1);var inst_10458 = inst_10457;var inst_10459 = null;var inst_10460 = 0;var inst_10461 = 0;var state_10522__$1 = (function (){var statearr_10539 = state_10522;(statearr_10539[10] = inst_10459);
(statearr_10539[20] = inst_10452);
(statearr_10539[19] = inst_10450__$1);
(statearr_10539[11] = inst_10460);
(statearr_10539[12] = inst_10461);
(statearr_10539[13] = inst_10458);
return statearr_10539;
})();var statearr_10540_10620 = state_10522__$1;(statearr_10540_10620[2] = null);
(statearr_10540_10620[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 38))
{var inst_10501 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10541_10621 = state_10522__$1;(statearr_10541_10621[2] = inst_10501);
(statearr_10541_10621[1] = 35);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 7))
{var inst_10518 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10542_10622 = state_10522__$1;(statearr_10542_10622[2] = inst_10518);
(statearr_10542_10622[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 39))
{var inst_10479 = (state_10522[9]);var inst_10497 = (state_10522[2]);var inst_10498 = cljs.core.next.call(null,inst_10479);var inst_10458 = inst_10498;var inst_10459 = null;var inst_10460 = 0;var inst_10461 = 0;var state_10522__$1 = (function (){var statearr_10543 = state_10522;(statearr_10543[10] = inst_10459);
(statearr_10543[21] = inst_10497);
(statearr_10543[11] = inst_10460);
(statearr_10543[12] = inst_10461);
(statearr_10543[13] = inst_10458);
return statearr_10543;
})();var statearr_10544_10623 = state_10522__$1;(statearr_10544_10623[2] = null);
(statearr_10544_10623[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 8))
{var inst_10401 = (state_10522[15]);var inst_10402 = (state_10522[16]);var inst_10404 = (inst_10402 < inst_10401);var inst_10405 = inst_10404;var state_10522__$1 = state_10522;if(cljs.core.truth_(inst_10405))
{var statearr_10545_10624 = state_10522__$1;(statearr_10545_10624[1] = 10);
} else
{var statearr_10546_10625 = state_10522__$1;(statearr_10546_10625[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 40))
{var inst_10488 = (state_10522[18]);var inst_10489 = (state_10522[2]);var inst_10490 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_10491 = cljs.core.async.untap_STAR_.call(null,m,inst_10488);var state_10522__$1 = (function (){var statearr_10547 = state_10522;(statearr_10547[22] = inst_10489);
(statearr_10547[23] = inst_10490);
return statearr_10547;
})();var statearr_10548_10626 = state_10522__$1;(statearr_10548_10626[2] = inst_10491);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10522__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 9))
{var inst_10447 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10549_10627 = state_10522__$1;(statearr_10549_10627[2] = inst_10447);
(statearr_10549_10627[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 41))
{var inst_10488 = (state_10522[18]);var inst_10390 = (state_10522[8]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_10522,40,Object,null,39);var inst_10495 = cljs.core.async.put_BANG_.call(null,inst_10488,inst_10390,done);var state_10522__$1 = state_10522;var statearr_10550_10628 = state_10522__$1;(statearr_10550_10628[2] = inst_10495);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10522__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 10))
{var inst_10400 = (state_10522[14]);var inst_10402 = (state_10522[16]);var inst_10408 = cljs.core._nth.call(null,inst_10400,inst_10402);var inst_10409 = cljs.core.nth.call(null,inst_10408,0,null);var inst_10410 = cljs.core.nth.call(null,inst_10408,1,null);var state_10522__$1 = (function (){var statearr_10551 = state_10522;(statearr_10551[24] = inst_10409);
return statearr_10551;
})();if(cljs.core.truth_(inst_10410))
{var statearr_10552_10629 = state_10522__$1;(statearr_10552_10629[1] = 13);
} else
{var statearr_10553_10630 = state_10522__$1;(statearr_10553_10630[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 42))
{var state_10522__$1 = state_10522;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_10522__$1,45,dchan);
} else
{if((state_val_10523 === 11))
{var inst_10419 = (state_10522[25]);var inst_10399 = (state_10522[17]);var inst_10419__$1 = cljs.core.seq.call(null,inst_10399);var state_10522__$1 = (function (){var statearr_10554 = state_10522;(statearr_10554[25] = inst_10419__$1);
return statearr_10554;
})();if(inst_10419__$1)
{var statearr_10555_10631 = state_10522__$1;(statearr_10555_10631[1] = 16);
} else
{var statearr_10556_10632 = state_10522__$1;(statearr_10556_10632[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 43))
{var state_10522__$1 = state_10522;var statearr_10557_10633 = state_10522__$1;(statearr_10557_10633[2] = null);
(statearr_10557_10633[1] = 44);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 12))
{var inst_10445 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10558_10634 = state_10522__$1;(statearr_10558_10634[2] = inst_10445);
(statearr_10558_10634[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 44))
{var inst_10515 = (state_10522[2]);var state_10522__$1 = (function (){var statearr_10559 = state_10522;(statearr_10559[26] = inst_10515);
return statearr_10559;
})();var statearr_10560_10635 = state_10522__$1;(statearr_10560_10635[2] = null);
(statearr_10560_10635[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 13))
{var inst_10409 = (state_10522[24]);var inst_10412 = cljs.core.async.close_BANG_.call(null,inst_10409);var state_10522__$1 = state_10522;var statearr_10561_10636 = state_10522__$1;(statearr_10561_10636[2] = inst_10412);
(statearr_10561_10636[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 45))
{var inst_10512 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10565_10637 = state_10522__$1;(statearr_10565_10637[2] = inst_10512);
(statearr_10565_10637[1] = 44);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 14))
{var state_10522__$1 = state_10522;var statearr_10566_10638 = state_10522__$1;(statearr_10566_10638[2] = null);
(statearr_10566_10638[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 15))
{var inst_10400 = (state_10522[14]);var inst_10401 = (state_10522[15]);var inst_10402 = (state_10522[16]);var inst_10399 = (state_10522[17]);var inst_10415 = (state_10522[2]);var inst_10416 = (inst_10402 + 1);var tmp10562 = inst_10400;var tmp10563 = inst_10401;var tmp10564 = inst_10399;var inst_10399__$1 = tmp10564;var inst_10400__$1 = tmp10562;var inst_10401__$1 = tmp10563;var inst_10402__$1 = inst_10416;var state_10522__$1 = (function (){var statearr_10567 = state_10522;(statearr_10567[27] = inst_10415);
(statearr_10567[14] = inst_10400__$1);
(statearr_10567[15] = inst_10401__$1);
(statearr_10567[16] = inst_10402__$1);
(statearr_10567[17] = inst_10399__$1);
return statearr_10567;
})();var statearr_10568_10639 = state_10522__$1;(statearr_10568_10639[2] = null);
(statearr_10568_10639[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 16))
{var inst_10419 = (state_10522[25]);var inst_10421 = cljs.core.chunked_seq_QMARK_.call(null,inst_10419);var state_10522__$1 = state_10522;if(inst_10421)
{var statearr_10569_10640 = state_10522__$1;(statearr_10569_10640[1] = 19);
} else
{var statearr_10570_10641 = state_10522__$1;(statearr_10570_10641[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 17))
{var state_10522__$1 = state_10522;var statearr_10571_10642 = state_10522__$1;(statearr_10571_10642[2] = null);
(statearr_10571_10642[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 18))
{var inst_10443 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10572_10643 = state_10522__$1;(statearr_10572_10643[2] = inst_10443);
(statearr_10572_10643[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 19))
{var inst_10419 = (state_10522[25]);var inst_10423 = cljs.core.chunk_first.call(null,inst_10419);var inst_10424 = cljs.core.chunk_rest.call(null,inst_10419);var inst_10425 = cljs.core.count.call(null,inst_10423);var inst_10399 = inst_10424;var inst_10400 = inst_10423;var inst_10401 = inst_10425;var inst_10402 = 0;var state_10522__$1 = (function (){var statearr_10573 = state_10522;(statearr_10573[14] = inst_10400);
(statearr_10573[15] = inst_10401);
(statearr_10573[16] = inst_10402);
(statearr_10573[17] = inst_10399);
return statearr_10573;
})();var statearr_10574_10644 = state_10522__$1;(statearr_10574_10644[2] = null);
(statearr_10574_10644[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 20))
{var inst_10419 = (state_10522[25]);var inst_10429 = cljs.core.first.call(null,inst_10419);var inst_10430 = cljs.core.nth.call(null,inst_10429,0,null);var inst_10431 = cljs.core.nth.call(null,inst_10429,1,null);var state_10522__$1 = (function (){var statearr_10575 = state_10522;(statearr_10575[28] = inst_10430);
return statearr_10575;
})();if(cljs.core.truth_(inst_10431))
{var statearr_10576_10645 = state_10522__$1;(statearr_10576_10645[1] = 22);
} else
{var statearr_10577_10646 = state_10522__$1;(statearr_10577_10646[1] = 23);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 21))
{var inst_10440 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10578_10647 = state_10522__$1;(statearr_10578_10647[2] = inst_10440);
(statearr_10578_10647[1] = 18);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 22))
{var inst_10430 = (state_10522[28]);var inst_10433 = cljs.core.async.close_BANG_.call(null,inst_10430);var state_10522__$1 = state_10522;var statearr_10579_10648 = state_10522__$1;(statearr_10579_10648[2] = inst_10433);
(statearr_10579_10648[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 23))
{var state_10522__$1 = state_10522;var statearr_10580_10649 = state_10522__$1;(statearr_10580_10649[2] = null);
(statearr_10580_10649[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 24))
{var inst_10419 = (state_10522[25]);var inst_10436 = (state_10522[2]);var inst_10437 = cljs.core.next.call(null,inst_10419);var inst_10399 = inst_10437;var inst_10400 = null;var inst_10401 = 0;var inst_10402 = 0;var state_10522__$1 = (function (){var statearr_10581 = state_10522;(statearr_10581[14] = inst_10400);
(statearr_10581[15] = inst_10401);
(statearr_10581[16] = inst_10402);
(statearr_10581[29] = inst_10436);
(statearr_10581[17] = inst_10399);
return statearr_10581;
})();var statearr_10582_10650 = state_10522__$1;(statearr_10582_10650[2] = null);
(statearr_10582_10650[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 25))
{var inst_10460 = (state_10522[11]);var inst_10461 = (state_10522[12]);var inst_10463 = (inst_10461 < inst_10460);var inst_10464 = inst_10463;var state_10522__$1 = state_10522;if(cljs.core.truth_(inst_10464))
{var statearr_10583_10651 = state_10522__$1;(statearr_10583_10651[1] = 27);
} else
{var statearr_10584_10652 = state_10522__$1;(statearr_10584_10652[1] = 28);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 26))
{var inst_10450 = (state_10522[19]);var inst_10508 = (state_10522[2]);var inst_10509 = cljs.core.seq.call(null,inst_10450);var state_10522__$1 = (function (){var statearr_10585 = state_10522;(statearr_10585[30] = inst_10508);
return statearr_10585;
})();if(inst_10509)
{var statearr_10586_10653 = state_10522__$1;(statearr_10586_10653[1] = 42);
} else
{var statearr_10587_10654 = state_10522__$1;(statearr_10587_10654[1] = 43);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 27))
{var inst_10459 = (state_10522[10]);var inst_10461 = (state_10522[12]);var inst_10466 = cljs.core._nth.call(null,inst_10459,inst_10461);var state_10522__$1 = (function (){var statearr_10588 = state_10522;(statearr_10588[7] = inst_10466);
return statearr_10588;
})();var statearr_10589_10655 = state_10522__$1;(statearr_10589_10655[2] = null);
(statearr_10589_10655[1] = 32);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 28))
{var inst_10479 = (state_10522[9]);var inst_10458 = (state_10522[13]);var inst_10479__$1 = cljs.core.seq.call(null,inst_10458);var state_10522__$1 = (function (){var statearr_10593 = state_10522;(statearr_10593[9] = inst_10479__$1);
return statearr_10593;
})();if(inst_10479__$1)
{var statearr_10594_10656 = state_10522__$1;(statearr_10594_10656[1] = 33);
} else
{var statearr_10595_10657 = state_10522__$1;(statearr_10595_10657[1] = 34);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 29))
{var inst_10506 = (state_10522[2]);var state_10522__$1 = state_10522;var statearr_10596_10658 = state_10522__$1;(statearr_10596_10658[2] = inst_10506);
(statearr_10596_10658[1] = 26);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 30))
{var inst_10459 = (state_10522[10]);var inst_10460 = (state_10522[11]);var inst_10461 = (state_10522[12]);var inst_10458 = (state_10522[13]);var inst_10475 = (state_10522[2]);var inst_10476 = (inst_10461 + 1);var tmp10590 = inst_10459;var tmp10591 = inst_10460;var tmp10592 = inst_10458;var inst_10458__$1 = tmp10592;var inst_10459__$1 = tmp10590;var inst_10460__$1 = tmp10591;var inst_10461__$1 = inst_10476;var state_10522__$1 = (function (){var statearr_10597 = state_10522;(statearr_10597[10] = inst_10459__$1);
(statearr_10597[11] = inst_10460__$1);
(statearr_10597[12] = inst_10461__$1);
(statearr_10597[31] = inst_10475);
(statearr_10597[13] = inst_10458__$1);
return statearr_10597;
})();var statearr_10598_10659 = state_10522__$1;(statearr_10598_10659[2] = null);
(statearr_10598_10659[1] = 25);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10523 === 31))
{var inst_10466 = (state_10522[7]);var inst_10467 = (state_10522[2]);var inst_10468 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var inst_10469 = cljs.core.async.untap_STAR_.call(null,m,inst_10466);var state_10522__$1 = (function (){var statearr_10599 = state_10522;(statearr_10599[32] = inst_10467);
(statearr_10599[33] = inst_10468);
return statearr_10599;
})();var statearr_10600_10660 = state_10522__$1;(statearr_10600_10660[2] = inst_10469);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10522__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___10608,cs,m,dchan,dctr,done))
;return ((function (switch__7040__auto__,c__7111__auto___10608,cs,m,dchan,dctr,done){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_10604 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_10604[0] = state_machine__7041__auto__);
(statearr_10604[1] = 1);
return statearr_10604;
});
var state_machine__7041__auto____1 = (function (state_10522){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_10522);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e10605){if((e10605 instanceof Object))
{var ex__7044__auto__ = e10605;var statearr_10606_10661 = state_10522;(statearr_10606_10661[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10522);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e10605;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__10662 = state_10522;
state_10522 = G__10662;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_10522){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_10522);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___10608,cs,m,dchan,dctr,done))
})();var state__7113__auto__ = (function (){var statearr_10607 = f__7112__auto__.call(null);(statearr_10607[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___10608);
return statearr_10607;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___10608,cs,m,dchan,dctr,done))
);
return m;
});
/**
* Copies the mult source onto the supplied channel.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.tap = (function() {
var tap = null;
var tap__2 = (function (mult,ch){return tap.call(null,mult,ch,true);
});
var tap__3 = (function (mult,ch,close_QMARK_){cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);
return ch;
});
tap = function(mult,ch,close_QMARK_){
switch(arguments.length){
case 2:
return tap__2.call(this,mult,ch);
case 3:
return tap__3.call(this,mult,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
tap.cljs$core$IFn$_invoke$arity$2 = tap__2;
tap.cljs$core$IFn$_invoke$arity$3 = tap__3;
return tap;
})()
;
/**
* Disconnects a target channel from a mult
*/
cljs.core.async.untap = (function untap(mult,ch){return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
* Disconnects all target channels from a mult
*/
cljs.core.async.untap_all = (function untap_all(mult){return cljs.core.async.untap_all_STAR_.call(null,mult);
});
cljs.core.async.Mix = (function (){var obj10664 = {};return obj10664;
})();
cljs.core.async.admix_STAR_ = (function admix_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$admix_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.admix_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_STAR_ = (function unmix_STAR_(m,ch){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
})().call(null,m,ch);
}
});
cljs.core.async.unmix_all_STAR_ = (function unmix_all_STAR_(m){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
})().call(null,m);
}
});
cljs.core.async.toggle_STAR_ = (function toggle_STAR_(m,state_map){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
})().call(null,m,state_map);
}
});
cljs.core.async.solo_mode_STAR_ = (function solo_mode_STAR_(m,mode){if((function (){var and__3526__auto__ = m;if(and__3526__auto__)
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else
{var x__4165__auto__ = (((m == null))?null:m);return (function (){var or__3538__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
})().call(null,m,mode);
}
});
/**
* Creates and returns a mix of one or more input channels which will
* be put on the supplied out channel. Input sources can be added to
* the mix with 'admix', and removed with 'unmix'. A mix supports
* soloing, muting and pausing multiple inputs atomically using
* 'toggle', and can solo using either muting or pausing as determined
* by 'solo-mode'.
* 
* Each channel can have zero or more boolean modes set via 'toggle':
* 
* :solo - when true, only this (ond other soloed) channel(s) will appear
* in the mix output channel. :mute and :pause states of soloed
* channels are ignored. If solo-mode is :mute, non-soloed
* channels are muted, if :pause, non-soloed channels are
* paused.
* 
* :mute - muted channels will have their contents consumed but not included in the mix
* :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
*/
cljs.core.async.mix = (function mix(out){var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",1120344424),null,new cljs.core.Keyword(null,"mute","mute",1017267595),null], null), null);var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",1017440337));var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595));var change = cljs.core.async.chan.call(null);var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){if(cljs.core.truth_(attr.call(null,v)))
{return cljs.core.conj.call(null,ret,c);
} else
{return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){var chs = cljs.core.deref.call(null,cs);var mode = cljs.core.deref.call(null,solo_mode);var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",1017440337),chs);var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",1120344424),chs);return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1123523302),solos,new cljs.core.Keyword(null,"mutes","mutes",1118168300),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1017267595),chs),new cljs.core.Keyword(null,"reads","reads",1122290959),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",1120344424))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;var m = (function (){if(typeof cljs.core.async.t10774 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t10774 = (function (pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,meta10775){
this.pick = pick;
this.out = out;
this.attrs = attrs;
this.cs = cs;
this.calc_state = calc_state;
this.solo_modes = solo_modes;
this.mix = mix;
this.changed = changed;
this.change = change;
this.solo_mode = solo_mode;
this.meta10775 = meta10775;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t10774.cljs$lang$type = true;
cljs.core.async.t10774.cljs$lang$ctorStr = "cljs.core.async/t10774";
cljs.core.async.t10774.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t10774");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$ = true;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){var self__ = this;
var ___$1 = this;cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){var self__ = this;
var ___$1 = this;if(cljs.core.truth_(self__.solo_modes.call(null,mode)))
{} else
{throw (new Error(("Assert failed: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(("mode must be one of: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)))+"\n"+cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.pr_str.call(null,cljs.core.list(new cljs.core.Symbol(null,"solo-modes","solo-modes",-1162732933,null),new cljs.core.Symbol(null,"mode","mode",-1637174436,null)))))));
}
cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);
return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t10774.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){var self__ = this;
var ___$1 = this;return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_10776){var self__ = this;
var _10776__$1 = this;return self__.meta10775;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.t10774.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_10776,meta10775__$1){var self__ = this;
var _10776__$1 = this;return (new cljs.core.async.t10774(self__.pick,self__.out,self__.attrs,self__.cs,self__.calc_state,self__.solo_modes,self__.mix,self__.changed,self__.change,self__.solo_mode,meta10775__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
cljs.core.async.__GT_t10774 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function __GT_t10774(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta10775){return (new cljs.core.async.t10774(pick__$1,out__$1,attrs__$1,cs__$1,calc_state__$1,solo_modes__$1,mix__$1,changed__$1,change__$1,solo_mode__$1,meta10775));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;
}
return (new cljs.core.async.t10774(pick,out,attrs,cs,calc_state,solo_modes,mix,changed,change,solo_mode,null));
})();var c__7111__auto___10883 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_10841){var state_val_10842 = (state_10841[1]);if((state_val_10842 === 1))
{var inst_10780 = (state_10841[7]);var inst_10780__$1 = calc_state.call(null);var inst_10781 = cljs.core.seq_QMARK_.call(null,inst_10780__$1);var state_10841__$1 = (function (){var statearr_10843 = state_10841;(statearr_10843[7] = inst_10780__$1);
return statearr_10843;
})();if(inst_10781)
{var statearr_10844_10884 = state_10841__$1;(statearr_10844_10884[1] = 2);
} else
{var statearr_10845_10885 = state_10841__$1;(statearr_10845_10885[1] = 3);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 2))
{var inst_10780 = (state_10841[7]);var inst_10783 = cljs.core.apply.call(null,cljs.core.hash_map,inst_10780);var state_10841__$1 = state_10841;var statearr_10846_10886 = state_10841__$1;(statearr_10846_10886[2] = inst_10783);
(statearr_10846_10886[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 3))
{var inst_10780 = (state_10841[7]);var state_10841__$1 = state_10841;var statearr_10847_10887 = state_10841__$1;(statearr_10847_10887[2] = inst_10780);
(statearr_10847_10887[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 4))
{var inst_10780 = (state_10841[7]);var inst_10786 = (state_10841[2]);var inst_10787 = cljs.core.get.call(null,inst_10786,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_10788 = cljs.core.get.call(null,inst_10786,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_10789 = cljs.core.get.call(null,inst_10786,new cljs.core.Keyword(null,"solos","solos",1123523302));var inst_10790 = inst_10780;var state_10841__$1 = (function (){var statearr_10848 = state_10841;(statearr_10848[8] = inst_10789);
(statearr_10848[9] = inst_10787);
(statearr_10848[10] = inst_10788);
(statearr_10848[11] = inst_10790);
return statearr_10848;
})();var statearr_10849_10888 = state_10841__$1;(statearr_10849_10888[2] = null);
(statearr_10849_10888[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 5))
{var inst_10790 = (state_10841[11]);var inst_10793 = cljs.core.seq_QMARK_.call(null,inst_10790);var state_10841__$1 = state_10841;if(inst_10793)
{var statearr_10850_10889 = state_10841__$1;(statearr_10850_10889[1] = 7);
} else
{var statearr_10851_10890 = state_10841__$1;(statearr_10851_10890[1] = 8);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 6))
{var inst_10839 = (state_10841[2]);var state_10841__$1 = state_10841;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_10841__$1,inst_10839);
} else
{if((state_val_10842 === 7))
{var inst_10790 = (state_10841[11]);var inst_10795 = cljs.core.apply.call(null,cljs.core.hash_map,inst_10790);var state_10841__$1 = state_10841;var statearr_10852_10891 = state_10841__$1;(statearr_10852_10891[2] = inst_10795);
(statearr_10852_10891[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 8))
{var inst_10790 = (state_10841[11]);var state_10841__$1 = state_10841;var statearr_10853_10892 = state_10841__$1;(statearr_10853_10892[2] = inst_10790);
(statearr_10853_10892[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 9))
{var inst_10798 = (state_10841[12]);var inst_10798__$1 = (state_10841[2]);var inst_10799 = cljs.core.get.call(null,inst_10798__$1,new cljs.core.Keyword(null,"reads","reads",1122290959));var inst_10800 = cljs.core.get.call(null,inst_10798__$1,new cljs.core.Keyword(null,"mutes","mutes",1118168300));var inst_10801 = cljs.core.get.call(null,inst_10798__$1,new cljs.core.Keyword(null,"solos","solos",1123523302));var state_10841__$1 = (function (){var statearr_10854 = state_10841;(statearr_10854[12] = inst_10798__$1);
(statearr_10854[13] = inst_10801);
(statearr_10854[14] = inst_10800);
return statearr_10854;
})();return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_10841__$1,10,inst_10799);
} else
{if((state_val_10842 === 10))
{var inst_10806 = (state_10841[15]);var inst_10805 = (state_10841[16]);var inst_10804 = (state_10841[2]);var inst_10805__$1 = cljs.core.nth.call(null,inst_10804,0,null);var inst_10806__$1 = cljs.core.nth.call(null,inst_10804,1,null);var inst_10807 = (inst_10805__$1 == null);var inst_10808 = cljs.core._EQ_.call(null,inst_10806__$1,change);var inst_10809 = (inst_10807) || (inst_10808);var state_10841__$1 = (function (){var statearr_10855 = state_10841;(statearr_10855[15] = inst_10806__$1);
(statearr_10855[16] = inst_10805__$1);
return statearr_10855;
})();if(cljs.core.truth_(inst_10809))
{var statearr_10856_10893 = state_10841__$1;(statearr_10856_10893[1] = 11);
} else
{var statearr_10857_10894 = state_10841__$1;(statearr_10857_10894[1] = 12);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 11))
{var inst_10805 = (state_10841[16]);var inst_10811 = (inst_10805 == null);var state_10841__$1 = state_10841;if(cljs.core.truth_(inst_10811))
{var statearr_10858_10895 = state_10841__$1;(statearr_10858_10895[1] = 14);
} else
{var statearr_10859_10896 = state_10841__$1;(statearr_10859_10896[1] = 15);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 12))
{var inst_10820 = (state_10841[17]);var inst_10801 = (state_10841[13]);var inst_10806 = (state_10841[15]);var inst_10820__$1 = inst_10801.call(null,inst_10806);var state_10841__$1 = (function (){var statearr_10860 = state_10841;(statearr_10860[17] = inst_10820__$1);
return statearr_10860;
})();if(cljs.core.truth_(inst_10820__$1))
{var statearr_10861_10897 = state_10841__$1;(statearr_10861_10897[1] = 17);
} else
{var statearr_10862_10898 = state_10841__$1;(statearr_10862_10898[1] = 18);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 13))
{var inst_10837 = (state_10841[2]);var state_10841__$1 = state_10841;var statearr_10863_10899 = state_10841__$1;(statearr_10863_10899[2] = inst_10837);
(statearr_10863_10899[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 14))
{var inst_10806 = (state_10841[15]);var inst_10813 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_10806);var state_10841__$1 = state_10841;var statearr_10864_10900 = state_10841__$1;(statearr_10864_10900[2] = inst_10813);
(statearr_10864_10900[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 15))
{var state_10841__$1 = state_10841;var statearr_10865_10901 = state_10841__$1;(statearr_10865_10901[2] = null);
(statearr_10865_10901[1] = 16);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 16))
{var inst_10816 = (state_10841[2]);var inst_10817 = calc_state.call(null);var inst_10790 = inst_10817;var state_10841__$1 = (function (){var statearr_10866 = state_10841;(statearr_10866[18] = inst_10816);
(statearr_10866[11] = inst_10790);
return statearr_10866;
})();var statearr_10867_10902 = state_10841__$1;(statearr_10867_10902[2] = null);
(statearr_10867_10902[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 17))
{var inst_10820 = (state_10841[17]);var state_10841__$1 = state_10841;var statearr_10868_10903 = state_10841__$1;(statearr_10868_10903[2] = inst_10820);
(statearr_10868_10903[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 18))
{var inst_10801 = (state_10841[13]);var inst_10800 = (state_10841[14]);var inst_10806 = (state_10841[15]);var inst_10823 = cljs.core.empty_QMARK_.call(null,inst_10801);var inst_10824 = inst_10800.call(null,inst_10806);var inst_10825 = cljs.core.not.call(null,inst_10824);var inst_10826 = (inst_10823) && (inst_10825);var state_10841__$1 = state_10841;var statearr_10869_10904 = state_10841__$1;(statearr_10869_10904[2] = inst_10826);
(statearr_10869_10904[1] = 19);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 19))
{var inst_10828 = (state_10841[2]);var state_10841__$1 = state_10841;if(cljs.core.truth_(inst_10828))
{var statearr_10870_10905 = state_10841__$1;(statearr_10870_10905[1] = 20);
} else
{var statearr_10871_10906 = state_10841__$1;(statearr_10871_10906[1] = 21);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 20))
{var inst_10805 = (state_10841[16]);var state_10841__$1 = state_10841;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_10841__$1,23,out,inst_10805);
} else
{if((state_val_10842 === 21))
{var state_10841__$1 = state_10841;var statearr_10872_10907 = state_10841__$1;(statearr_10872_10907[2] = null);
(statearr_10872_10907[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 22))
{var inst_10798 = (state_10841[12]);var inst_10834 = (state_10841[2]);var inst_10790 = inst_10798;var state_10841__$1 = (function (){var statearr_10873 = state_10841;(statearr_10873[19] = inst_10834);
(statearr_10873[11] = inst_10790);
return statearr_10873;
})();var statearr_10874_10908 = state_10841__$1;(statearr_10874_10908[2] = null);
(statearr_10874_10908[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_10842 === 23))
{var inst_10831 = (state_10841[2]);var state_10841__$1 = state_10841;var statearr_10875_10909 = state_10841__$1;(statearr_10875_10909[2] = inst_10831);
(statearr_10875_10909[1] = 22);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;return ((function (switch__7040__auto__,c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_10879 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_10879[0] = state_machine__7041__auto__);
(statearr_10879[1] = 1);
return statearr_10879;
});
var state_machine__7041__auto____1 = (function (state_10841){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_10841);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e10880){if((e10880 instanceof Object))
{var ex__7044__auto__ = e10880;var statearr_10881_10910 = state_10841;(statearr_10881_10910[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_10841);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e10880;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__10911 = state_10841;
state_10841 = G__10911;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_10841){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_10841);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();var state__7113__auto__ = (function (){var statearr_10882 = f__7112__auto__.call(null);(statearr_10882[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___10883);
return statearr_10882;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___10883,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);
return m;
});
/**
* Adds ch as an input to the mix
*/
cljs.core.async.admix = (function admix(mix,ch){return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
* Removes ch as an input to the mix
*/
cljs.core.async.unmix = (function unmix(mix,ch){return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
* removes all inputs from the mix
*/
cljs.core.async.unmix_all = (function unmix_all(mix){return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
* Atomically sets the state(s) of one or more channels in a mix. The
* state map is a map of channels -> channel-state-map. A
* channel-state-map is a map of attrs -> boolean, where attr is one or
* more of :mute, :pause or :solo. Any states supplied are merged with
* the current state.
* 
* Note that channels can be added to a mix via toggle, which can be
* used to add channels in a particular (e.g. paused) state.
*/
cljs.core.async.toggle = (function toggle(mix,state_map){return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
* Sets the solo mode of the mix. mode must be one of :mute or :pause
*/
cljs.core.async.solo_mode = (function solo_mode(mix,mode){return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});
cljs.core.async.Pub = (function (){var obj10913 = {};return obj10913;
})();
cljs.core.async.sub_STAR_ = (function sub_STAR_(p,v,ch,close_QMARK_){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$sub_STAR_$arity$4;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.sub_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
})().call(null,p,v,ch,close_QMARK_);
}
});
cljs.core.async.unsub_STAR_ = (function unsub_STAR_(p,v,ch){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
})().call(null,p,v,ch);
}
});
cljs.core.async.unsub_all_STAR_ = (function() {
var unsub_all_STAR_ = null;
var unsub_all_STAR___1 = (function (p){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p);
}
});
var unsub_all_STAR___2 = (function (p,v){if((function (){var and__3526__auto__ = p;if(and__3526__auto__)
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2;
} else
{return and__3526__auto__;
}
})())
{return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else
{var x__4165__auto__ = (((p == null))?null:p);return (function (){var or__3538__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__4165__auto__)]);if(or__3538__auto__)
{return or__3538__auto__;
} else
{var or__3538__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);if(or__3538__auto____$1)
{return or__3538__auto____$1;
} else
{throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
})().call(null,p,v);
}
});
unsub_all_STAR_ = function(p,v){
switch(arguments.length){
case 1:
return unsub_all_STAR___1.call(this,p);
case 2:
return unsub_all_STAR___2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = unsub_all_STAR___1;
unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = unsub_all_STAR___2;
return unsub_all_STAR_;
})()
;
/**
* Creates and returns a pub(lication) of the supplied channel,
* partitioned into topics by the topic-fn. topic-fn will be applied to
* each value on the channel and the result will determine the 'topic'
* on which that value will be put. Channels can be subscribed to
* receive copies of topics using 'sub', and unsubscribed using
* 'unsub'. Each topic will be handled by an internal mult on a
* dedicated channel. By default these internal channels are
* unbuffered, but a buf-fn can be supplied which, given a topic,
* creates a buffer with desired properties.
* 
* Each item is distributed to all subs in parallel and synchronously,
* i.e. each sub must accept before the next item is distributed. Use
* buffering/windowing to prevent slow subs from holding up the pub.
* 
* Items received when there are no matching subs get dropped.
* 
* Note that if buf-fns are used then each topic is handled
* asynchronously, i.e. if a channel is subscribed to more than one
* topic it should not expect them to be interleaved identically with
* the source.
*/
cljs.core.async.pub = (function() {
var pub = null;
var pub__2 = (function (ch,topic_fn){return pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});
var pub__3 = (function (ch,topic_fn,buf_fn){var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);var ensure_mult = ((function (mults){
return (function (topic){var or__3538__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);if(cljs.core.truth_(or__3538__auto__))
{return or__3538__auto__;
} else
{return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__3538__auto__,mults){
return (function (p1__10914_SHARP_){if(cljs.core.truth_(p1__10914_SHARP_.call(null,topic)))
{return p1__10914_SHARP_;
} else
{return cljs.core.assoc.call(null,p1__10914_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__3538__auto__,mults))
),topic);
}
});})(mults))
;var p = (function (){if(typeof cljs.core.async.t11039 !== 'undefined')
{} else
{
/**
* @constructor
*/
cljs.core.async.t11039 = (function (ensure_mult,mults,buf_fn,topic_fn,ch,pub,meta11040){
this.ensure_mult = ensure_mult;
this.mults = mults;
this.buf_fn = buf_fn;
this.topic_fn = topic_fn;
this.ch = ch;
this.pub = pub;
this.meta11040 = meta11040;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
cljs.core.async.t11039.cljs$lang$type = true;
cljs.core.async.t11039.cljs$lang$ctorStr = "cljs.core.async/t11039";
cljs.core.async.t11039.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"cljs.core.async/t11039");
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$async$Pub$ = true;
cljs.core.async.t11039.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2,close_QMARK_){var self__ = this;
var p__$1 = this;var m = self__.ensure_mult.call(null,topic);return cljs.core.async.tap.call(null,m,ch__$2,close_QMARK_);
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$2){var self__ = this;
var p__$1 = this;var temp__4092__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);if(cljs.core.truth_(temp__4092__auto__))
{var m = temp__4092__auto__;return cljs.core.async.untap.call(null,m,ch__$2);
} else
{return null;
}
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){var self__ = this;
var ___$1 = this;return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$async$Mux$ = true;
cljs.core.async.t11039.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){var self__ = this;
var ___$1 = this;return self__.ch;
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_11041){var self__ = this;
var _11041__$1 = this;return self__.meta11040;
});})(mults,ensure_mult))
;
cljs.core.async.t11039.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_11041,meta11040__$1){var self__ = this;
var _11041__$1 = this;return (new cljs.core.async.t11039(self__.ensure_mult,self__.mults,self__.buf_fn,self__.topic_fn,self__.ch,self__.pub,meta11040__$1));
});})(mults,ensure_mult))
;
cljs.core.async.__GT_t11039 = ((function (mults,ensure_mult){
return (function __GT_t11039(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta11040){return (new cljs.core.async.t11039(ensure_mult__$1,mults__$1,buf_fn__$1,topic_fn__$1,ch__$1,pub__$1,meta11040));
});})(mults,ensure_mult))
;
}
return (new cljs.core.async.t11039(ensure_mult,mults,buf_fn,topic_fn,ch,pub,null));
})();var c__7111__auto___11163 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11163,mults,ensure_mult,p){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11163,mults,ensure_mult,p){
return (function (state_11115){var state_val_11116 = (state_11115[1]);if((state_val_11116 === 1))
{var state_11115__$1 = state_11115;var statearr_11117_11164 = state_11115__$1;(statearr_11117_11164[2] = null);
(statearr_11117_11164[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 2))
{var state_11115__$1 = state_11115;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11115__$1,4,ch);
} else
{if((state_val_11116 === 3))
{var inst_11113 = (state_11115[2]);var state_11115__$1 = state_11115;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11115__$1,inst_11113);
} else
{if((state_val_11116 === 4))
{var inst_11044 = (state_11115[7]);var inst_11044__$1 = (state_11115[2]);var inst_11045 = (inst_11044__$1 == null);var state_11115__$1 = (function (){var statearr_11118 = state_11115;(statearr_11118[7] = inst_11044__$1);
return statearr_11118;
})();if(cljs.core.truth_(inst_11045))
{var statearr_11119_11165 = state_11115__$1;(statearr_11119_11165[1] = 5);
} else
{var statearr_11120_11166 = state_11115__$1;(statearr_11120_11166[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 5))
{var inst_11051 = cljs.core.deref.call(null,mults);var inst_11052 = cljs.core.vals.call(null,inst_11051);var inst_11053 = cljs.core.seq.call(null,inst_11052);var inst_11054 = inst_11053;var inst_11055 = null;var inst_11056 = 0;var inst_11057 = 0;var state_11115__$1 = (function (){var statearr_11121 = state_11115;(statearr_11121[8] = inst_11054);
(statearr_11121[9] = inst_11055);
(statearr_11121[10] = inst_11057);
(statearr_11121[11] = inst_11056);
return statearr_11121;
})();var statearr_11122_11167 = state_11115__$1;(statearr_11122_11167[2] = null);
(statearr_11122_11167[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 6))
{var inst_11092 = (state_11115[12]);var inst_11094 = (state_11115[13]);var inst_11044 = (state_11115[7]);var inst_11092__$1 = topic_fn.call(null,inst_11044);var inst_11093 = cljs.core.deref.call(null,mults);var inst_11094__$1 = cljs.core.get.call(null,inst_11093,inst_11092__$1);var state_11115__$1 = (function (){var statearr_11123 = state_11115;(statearr_11123[12] = inst_11092__$1);
(statearr_11123[13] = inst_11094__$1);
return statearr_11123;
})();if(cljs.core.truth_(inst_11094__$1))
{var statearr_11124_11168 = state_11115__$1;(statearr_11124_11168[1] = 19);
} else
{var statearr_11125_11169 = state_11115__$1;(statearr_11125_11169[1] = 20);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 7))
{var inst_11111 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11126_11170 = state_11115__$1;(statearr_11126_11170[2] = inst_11111);
(statearr_11126_11170[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 8))
{var inst_11057 = (state_11115[10]);var inst_11056 = (state_11115[11]);var inst_11059 = (inst_11057 < inst_11056);var inst_11060 = inst_11059;var state_11115__$1 = state_11115;if(cljs.core.truth_(inst_11060))
{var statearr_11130_11171 = state_11115__$1;(statearr_11130_11171[1] = 10);
} else
{var statearr_11131_11172 = state_11115__$1;(statearr_11131_11172[1] = 11);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 9))
{var inst_11090 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11132_11173 = state_11115__$1;(statearr_11132_11173[2] = inst_11090);
(statearr_11132_11173[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 10))
{var inst_11054 = (state_11115[8]);var inst_11055 = (state_11115[9]);var inst_11057 = (state_11115[10]);var inst_11056 = (state_11115[11]);var inst_11062 = cljs.core._nth.call(null,inst_11055,inst_11057);var inst_11063 = cljs.core.async.muxch_STAR_.call(null,inst_11062);var inst_11064 = cljs.core.async.close_BANG_.call(null,inst_11063);var inst_11065 = (inst_11057 + 1);var tmp11127 = inst_11054;var tmp11128 = inst_11055;var tmp11129 = inst_11056;var inst_11054__$1 = tmp11127;var inst_11055__$1 = tmp11128;var inst_11056__$1 = tmp11129;var inst_11057__$1 = inst_11065;var state_11115__$1 = (function (){var statearr_11133 = state_11115;(statearr_11133[8] = inst_11054__$1);
(statearr_11133[9] = inst_11055__$1);
(statearr_11133[14] = inst_11064);
(statearr_11133[10] = inst_11057__$1);
(statearr_11133[11] = inst_11056__$1);
return statearr_11133;
})();var statearr_11134_11174 = state_11115__$1;(statearr_11134_11174[2] = null);
(statearr_11134_11174[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 11))
{var inst_11054 = (state_11115[8]);var inst_11068 = (state_11115[15]);var inst_11068__$1 = cljs.core.seq.call(null,inst_11054);var state_11115__$1 = (function (){var statearr_11135 = state_11115;(statearr_11135[15] = inst_11068__$1);
return statearr_11135;
})();if(inst_11068__$1)
{var statearr_11136_11175 = state_11115__$1;(statearr_11136_11175[1] = 13);
} else
{var statearr_11137_11176 = state_11115__$1;(statearr_11137_11176[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 12))
{var inst_11088 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11138_11177 = state_11115__$1;(statearr_11138_11177[2] = inst_11088);
(statearr_11138_11177[1] = 9);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 13))
{var inst_11068 = (state_11115[15]);var inst_11070 = cljs.core.chunked_seq_QMARK_.call(null,inst_11068);var state_11115__$1 = state_11115;if(inst_11070)
{var statearr_11139_11178 = state_11115__$1;(statearr_11139_11178[1] = 16);
} else
{var statearr_11140_11179 = state_11115__$1;(statearr_11140_11179[1] = 17);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 14))
{var state_11115__$1 = state_11115;var statearr_11141_11180 = state_11115__$1;(statearr_11141_11180[2] = null);
(statearr_11141_11180[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 15))
{var inst_11086 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11142_11181 = state_11115__$1;(statearr_11142_11181[2] = inst_11086);
(statearr_11142_11181[1] = 12);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 16))
{var inst_11068 = (state_11115[15]);var inst_11072 = cljs.core.chunk_first.call(null,inst_11068);var inst_11073 = cljs.core.chunk_rest.call(null,inst_11068);var inst_11074 = cljs.core.count.call(null,inst_11072);var inst_11054 = inst_11073;var inst_11055 = inst_11072;var inst_11056 = inst_11074;var inst_11057 = 0;var state_11115__$1 = (function (){var statearr_11143 = state_11115;(statearr_11143[8] = inst_11054);
(statearr_11143[9] = inst_11055);
(statearr_11143[10] = inst_11057);
(statearr_11143[11] = inst_11056);
return statearr_11143;
})();var statearr_11144_11182 = state_11115__$1;(statearr_11144_11182[2] = null);
(statearr_11144_11182[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 17))
{var inst_11068 = (state_11115[15]);var inst_11077 = cljs.core.first.call(null,inst_11068);var inst_11078 = cljs.core.async.muxch_STAR_.call(null,inst_11077);var inst_11079 = cljs.core.async.close_BANG_.call(null,inst_11078);var inst_11080 = cljs.core.next.call(null,inst_11068);var inst_11054 = inst_11080;var inst_11055 = null;var inst_11056 = 0;var inst_11057 = 0;var state_11115__$1 = (function (){var statearr_11145 = state_11115;(statearr_11145[8] = inst_11054);
(statearr_11145[9] = inst_11055);
(statearr_11145[10] = inst_11057);
(statearr_11145[11] = inst_11056);
(statearr_11145[16] = inst_11079);
return statearr_11145;
})();var statearr_11146_11183 = state_11115__$1;(statearr_11146_11183[2] = null);
(statearr_11146_11183[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 18))
{var inst_11083 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11147_11184 = state_11115__$1;(statearr_11147_11184[2] = inst_11083);
(statearr_11147_11184[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 19))
{var state_11115__$1 = state_11115;var statearr_11148_11185 = state_11115__$1;(statearr_11148_11185[2] = null);
(statearr_11148_11185[1] = 24);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 20))
{var state_11115__$1 = state_11115;var statearr_11149_11186 = state_11115__$1;(statearr_11149_11186[2] = null);
(statearr_11149_11186[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 21))
{var inst_11108 = (state_11115[2]);var state_11115__$1 = (function (){var statearr_11150 = state_11115;(statearr_11150[17] = inst_11108);
return statearr_11150;
})();var statearr_11151_11187 = state_11115__$1;(statearr_11151_11187[2] = null);
(statearr_11151_11187[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 22))
{var inst_11105 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11152_11188 = state_11115__$1;(statearr_11152_11188[2] = inst_11105);
(statearr_11152_11188[1] = 21);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 23))
{var inst_11092 = (state_11115[12]);var inst_11096 = (state_11115[2]);var inst_11097 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_11092);var state_11115__$1 = (function (){var statearr_11153 = state_11115;(statearr_11153[18] = inst_11096);
return statearr_11153;
})();var statearr_11154_11189 = state_11115__$1;(statearr_11154_11189[2] = inst_11097);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11115__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11116 === 24))
{var inst_11094 = (state_11115[13]);var inst_11044 = (state_11115[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_11115,23,Object,null,22);var inst_11101 = cljs.core.async.muxch_STAR_.call(null,inst_11094);var state_11115__$1 = state_11115;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11115__$1,25,inst_11101,inst_11044);
} else
{if((state_val_11116 === 25))
{var inst_11103 = (state_11115[2]);var state_11115__$1 = state_11115;var statearr_11155_11190 = state_11115__$1;(statearr_11155_11190[2] = inst_11103);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11115__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11163,mults,ensure_mult,p))
;return ((function (switch__7040__auto__,c__7111__auto___11163,mults,ensure_mult,p){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11159 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11159[0] = state_machine__7041__auto__);
(statearr_11159[1] = 1);
return statearr_11159;
});
var state_machine__7041__auto____1 = (function (state_11115){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11115);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11160){if((e11160 instanceof Object))
{var ex__7044__auto__ = e11160;var statearr_11161_11191 = state_11115;(statearr_11161_11191[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11115);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11160;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11192 = state_11115;
state_11115 = G__11192;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11115){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11115);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11163,mults,ensure_mult,p))
})();var state__7113__auto__ = (function (){var statearr_11162 = f__7112__auto__.call(null);(statearr_11162[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11163);
return statearr_11162;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11163,mults,ensure_mult,p))
);
return p;
});
pub = function(ch,topic_fn,buf_fn){
switch(arguments.length){
case 2:
return pub__2.call(this,ch,topic_fn);
case 3:
return pub__3.call(this,ch,topic_fn,buf_fn);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
pub.cljs$core$IFn$_invoke$arity$2 = pub__2;
pub.cljs$core$IFn$_invoke$arity$3 = pub__3;
return pub;
})()
;
/**
* Subscribes a channel to a topic of a pub.
* 
* By default the channel will be closed when the source closes,
* but can be determined by the close? parameter.
*/
cljs.core.async.sub = (function() {
var sub = null;
var sub__3 = (function (p,topic,ch){return sub.call(null,p,topic,ch,true);
});
var sub__4 = (function (p,topic,ch,close_QMARK_){return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});
sub = function(p,topic,ch,close_QMARK_){
switch(arguments.length){
case 3:
return sub__3.call(this,p,topic,ch);
case 4:
return sub__4.call(this,p,topic,ch,close_QMARK_);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
sub.cljs$core$IFn$_invoke$arity$3 = sub__3;
sub.cljs$core$IFn$_invoke$arity$4 = sub__4;
return sub;
})()
;
/**
* Unsubscribes a channel from a topic of a pub
*/
cljs.core.async.unsub = (function unsub(p,topic,ch){return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
* Unsubscribes all channels from a pub, or a topic of a pub
*/
cljs.core.async.unsub_all = (function() {
var unsub_all = null;
var unsub_all__1 = (function (p){return cljs.core.async.unsub_all_STAR_.call(null,p);
});
var unsub_all__2 = (function (p,topic){return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});
unsub_all = function(p,topic){
switch(arguments.length){
case 1:
return unsub_all__1.call(this,p);
case 2:
return unsub_all__2.call(this,p,topic);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unsub_all.cljs$core$IFn$_invoke$arity$1 = unsub_all__1;
unsub_all.cljs$core$IFn$_invoke$arity$2 = unsub_all__2;
return unsub_all;
})()
;
/**
* Takes a function and a collection of source channels, and returns a
* channel which contains the values produced by applying f to the set
* of first items taken from each source channel, followed by applying
* f to the set of second items from each channel, until any one of the
* channels is closed, at which point the output channel will be
* closed. The returned channel will be unbuffered by default, or a
* buf-or-n can be supplied
*/
cljs.core.async.map = (function() {
var map = null;
var map__2 = (function (f,chs){return map.call(null,f,chs,null);
});
var map__3 = (function (f,chs,buf_or_n){var chs__$1 = cljs.core.vec.call(null,chs);var out = cljs.core.async.chan.call(null,buf_or_n);var cnt = cljs.core.count.call(null,chs__$1);var rets = cljs.core.object_array.call(null,cnt);var dchan = cljs.core.async.chan.call(null,1);var dctr = cljs.core.atom.call(null,null);var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){(rets[i] = ret);
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === 0))
{return cljs.core.async.put_BANG_.call(null,dchan,rets.slice(0));
} else
{return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));var c__7111__auto___11329 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_11299){var state_val_11300 = (state_11299[1]);if((state_val_11300 === 1))
{var state_11299__$1 = state_11299;var statearr_11301_11330 = state_11299__$1;(statearr_11301_11330[2] = null);
(statearr_11301_11330[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 2))
{var inst_11262 = cljs.core.reset_BANG_.call(null,dctr,cnt);var inst_11263 = 0;var state_11299__$1 = (function (){var statearr_11302 = state_11299;(statearr_11302[7] = inst_11263);
(statearr_11302[8] = inst_11262);
return statearr_11302;
})();var statearr_11303_11331 = state_11299__$1;(statearr_11303_11331[2] = null);
(statearr_11303_11331[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 3))
{var inst_11297 = (state_11299[2]);var state_11299__$1 = state_11299;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11299__$1,inst_11297);
} else
{if((state_val_11300 === 4))
{var inst_11263 = (state_11299[7]);var inst_11265 = (inst_11263 < cnt);var state_11299__$1 = state_11299;if(cljs.core.truth_(inst_11265))
{var statearr_11304_11332 = state_11299__$1;(statearr_11304_11332[1] = 6);
} else
{var statearr_11305_11333 = state_11299__$1;(statearr_11305_11333[1] = 7);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 5))
{var inst_11283 = (state_11299[2]);var state_11299__$1 = (function (){var statearr_11306 = state_11299;(statearr_11306[9] = inst_11283);
return statearr_11306;
})();return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11299__$1,12,dchan);
} else
{if((state_val_11300 === 6))
{var state_11299__$1 = state_11299;var statearr_11307_11334 = state_11299__$1;(statearr_11307_11334[2] = null);
(statearr_11307_11334[1] = 11);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 7))
{var state_11299__$1 = state_11299;var statearr_11308_11335 = state_11299__$1;(statearr_11308_11335[2] = null);
(statearr_11308_11335[1] = 8);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 8))
{var inst_11281 = (state_11299[2]);var state_11299__$1 = state_11299;var statearr_11309_11336 = state_11299__$1;(statearr_11309_11336[2] = inst_11281);
(statearr_11309_11336[1] = 5);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 9))
{var inst_11263 = (state_11299[7]);var inst_11276 = (state_11299[2]);var inst_11277 = (inst_11263 + 1);var inst_11263__$1 = inst_11277;var state_11299__$1 = (function (){var statearr_11310 = state_11299;(statearr_11310[7] = inst_11263__$1);
(statearr_11310[10] = inst_11276);
return statearr_11310;
})();var statearr_11311_11337 = state_11299__$1;(statearr_11311_11337[2] = null);
(statearr_11311_11337[1] = 4);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 10))
{var inst_11267 = (state_11299[2]);var inst_11268 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);var state_11299__$1 = (function (){var statearr_11312 = state_11299;(statearr_11312[11] = inst_11267);
return statearr_11312;
})();var statearr_11313_11338 = state_11299__$1;(statearr_11313_11338[2] = inst_11268);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11299__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 11))
{var inst_11263 = (state_11299[7]);var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_11299,10,Object,null,9);var inst_11272 = chs__$1.call(null,inst_11263);var inst_11273 = done.call(null,inst_11263);var inst_11274 = cljs.core.async.take_BANG_.call(null,inst_11272,inst_11273);var state_11299__$1 = state_11299;var statearr_11314_11339 = state_11299__$1;(statearr_11314_11339[2] = inst_11274);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11299__$1);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 12))
{var inst_11285 = (state_11299[12]);var inst_11285__$1 = (state_11299[2]);var inst_11286 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_11285__$1);var state_11299__$1 = (function (){var statearr_11315 = state_11299;(statearr_11315[12] = inst_11285__$1);
return statearr_11315;
})();if(cljs.core.truth_(inst_11286))
{var statearr_11316_11340 = state_11299__$1;(statearr_11316_11340[1] = 13);
} else
{var statearr_11317_11341 = state_11299__$1;(statearr_11317_11341[1] = 14);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 13))
{var inst_11288 = cljs.core.async.close_BANG_.call(null,out);var state_11299__$1 = state_11299;var statearr_11318_11342 = state_11299__$1;(statearr_11318_11342[2] = inst_11288);
(statearr_11318_11342[1] = 15);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 14))
{var inst_11285 = (state_11299[12]);var inst_11290 = cljs.core.apply.call(null,f,inst_11285);var state_11299__$1 = state_11299;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11299__$1,16,out,inst_11290);
} else
{if((state_val_11300 === 15))
{var inst_11295 = (state_11299[2]);var state_11299__$1 = state_11299;var statearr_11319_11343 = state_11299__$1;(statearr_11319_11343[2] = inst_11295);
(statearr_11319_11343[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11300 === 16))
{var inst_11292 = (state_11299[2]);var state_11299__$1 = (function (){var statearr_11320 = state_11299;(statearr_11320[13] = inst_11292);
return statearr_11320;
})();var statearr_11321_11344 = state_11299__$1;(statearr_11321_11344[2] = null);
(statearr_11321_11344[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done))
;return ((function (switch__7040__auto__,c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11325 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11325[0] = state_machine__7041__auto__);
(statearr_11325[1] = 1);
return statearr_11325;
});
var state_machine__7041__auto____1 = (function (state_11299){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11299);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11326){if((e11326 instanceof Object))
{var ex__7044__auto__ = e11326;var statearr_11327_11345 = state_11299;(statearr_11327_11345[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11299);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11326;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11346 = state_11299;
state_11299 = G__11346;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11299){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11299);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done))
})();var state__7113__auto__ = (function (){var statearr_11328 = f__7112__auto__.call(null);(statearr_11328[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11329);
return statearr_11328;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11329,chs__$1,out,cnt,rets,dchan,dctr,done))
);
return out;
});
map = function(f,chs,buf_or_n){
switch(arguments.length){
case 2:
return map__2.call(this,f,chs);
case 3:
return map__3.call(this,f,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
map.cljs$core$IFn$_invoke$arity$2 = map__2;
map.cljs$core$IFn$_invoke$arity$3 = map__3;
return map;
})()
;
/**
* Takes a collection of source channels and returns a channel which
* contains all values taken from them. The returned channel will be
* unbuffered by default, or a buf-or-n can be supplied. The channel
* will close after all the source channels have closed.
*/
cljs.core.async.merge = (function() {
var merge = null;
var merge__1 = (function (chs){return merge.call(null,chs,null);
});
var merge__2 = (function (chs,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___11454 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11454,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11454,out){
return (function (state_11430){var state_val_11431 = (state_11430[1]);if((state_val_11431 === 1))
{var inst_11401 = cljs.core.vec.call(null,chs);var inst_11402 = inst_11401;var state_11430__$1 = (function (){var statearr_11432 = state_11430;(statearr_11432[7] = inst_11402);
return statearr_11432;
})();var statearr_11433_11455 = state_11430__$1;(statearr_11433_11455[2] = null);
(statearr_11433_11455[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 2))
{var inst_11402 = (state_11430[7]);var inst_11404 = cljs.core.count.call(null,inst_11402);var inst_11405 = (inst_11404 > 0);var state_11430__$1 = state_11430;if(cljs.core.truth_(inst_11405))
{var statearr_11434_11456 = state_11430__$1;(statearr_11434_11456[1] = 4);
} else
{var statearr_11435_11457 = state_11430__$1;(statearr_11435_11457[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 3))
{var inst_11428 = (state_11430[2]);var state_11430__$1 = state_11430;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11430__$1,inst_11428);
} else
{if((state_val_11431 === 4))
{var inst_11402 = (state_11430[7]);var state_11430__$1 = state_11430;return cljs.core.async.impl.ioc_helpers.ioc_alts_BANG_.call(null,state_11430__$1,7,inst_11402);
} else
{if((state_val_11431 === 5))
{var inst_11424 = cljs.core.async.close_BANG_.call(null,out);var state_11430__$1 = state_11430;var statearr_11436_11458 = state_11430__$1;(statearr_11436_11458[2] = inst_11424);
(statearr_11436_11458[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 6))
{var inst_11426 = (state_11430[2]);var state_11430__$1 = state_11430;var statearr_11437_11459 = state_11430__$1;(statearr_11437_11459[2] = inst_11426);
(statearr_11437_11459[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 7))
{var inst_11410 = (state_11430[8]);var inst_11409 = (state_11430[9]);var inst_11409__$1 = (state_11430[2]);var inst_11410__$1 = cljs.core.nth.call(null,inst_11409__$1,0,null);var inst_11411 = cljs.core.nth.call(null,inst_11409__$1,1,null);var inst_11412 = (inst_11410__$1 == null);var state_11430__$1 = (function (){var statearr_11438 = state_11430;(statearr_11438[10] = inst_11411);
(statearr_11438[8] = inst_11410__$1);
(statearr_11438[9] = inst_11409__$1);
return statearr_11438;
})();if(cljs.core.truth_(inst_11412))
{var statearr_11439_11460 = state_11430__$1;(statearr_11439_11460[1] = 8);
} else
{var statearr_11440_11461 = state_11430__$1;(statearr_11440_11461[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 8))
{var inst_11411 = (state_11430[10]);var inst_11402 = (state_11430[7]);var inst_11410 = (state_11430[8]);var inst_11409 = (state_11430[9]);var inst_11414 = (function (){var c = inst_11411;var v = inst_11410;var vec__11407 = inst_11409;var cs = inst_11402;return ((function (c,v,vec__11407,cs,inst_11411,inst_11402,inst_11410,inst_11409,state_val_11431,c__7111__auto___11454,out){
return (function (p1__11347_SHARP_){return cljs.core.not_EQ_.call(null,c,p1__11347_SHARP_);
});
;})(c,v,vec__11407,cs,inst_11411,inst_11402,inst_11410,inst_11409,state_val_11431,c__7111__auto___11454,out))
})();var inst_11415 = cljs.core.filterv.call(null,inst_11414,inst_11402);var inst_11402__$1 = inst_11415;var state_11430__$1 = (function (){var statearr_11441 = state_11430;(statearr_11441[7] = inst_11402__$1);
return statearr_11441;
})();var statearr_11442_11462 = state_11430__$1;(statearr_11442_11462[2] = null);
(statearr_11442_11462[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 9))
{var inst_11410 = (state_11430[8]);var state_11430__$1 = state_11430;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11430__$1,11,out,inst_11410);
} else
{if((state_val_11431 === 10))
{var inst_11422 = (state_11430[2]);var state_11430__$1 = state_11430;var statearr_11444_11463 = state_11430__$1;(statearr_11444_11463[2] = inst_11422);
(statearr_11444_11463[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11431 === 11))
{var inst_11402 = (state_11430[7]);var inst_11419 = (state_11430[2]);var tmp11443 = inst_11402;var inst_11402__$1 = tmp11443;var state_11430__$1 = (function (){var statearr_11445 = state_11430;(statearr_11445[7] = inst_11402__$1);
(statearr_11445[11] = inst_11419);
return statearr_11445;
})();var statearr_11446_11464 = state_11430__$1;(statearr_11446_11464[2] = null);
(statearr_11446_11464[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11454,out))
;return ((function (switch__7040__auto__,c__7111__auto___11454,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11450 = [null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11450[0] = state_machine__7041__auto__);
(statearr_11450[1] = 1);
return statearr_11450;
});
var state_machine__7041__auto____1 = (function (state_11430){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11430);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11451){if((e11451 instanceof Object))
{var ex__7044__auto__ = e11451;var statearr_11452_11465 = state_11430;(statearr_11452_11465[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11430);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11451;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11466 = state_11430;
state_11430 = G__11466;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11430){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11430);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11454,out))
})();var state__7113__auto__ = (function (){var statearr_11453 = f__7112__auto__.call(null);(statearr_11453[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11454);
return statearr_11453;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11454,out))
);
return out;
});
merge = function(chs,buf_or_n){
switch(arguments.length){
case 1:
return merge__1.call(this,chs);
case 2:
return merge__2.call(this,chs,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
merge.cljs$core$IFn$_invoke$arity$1 = merge__1;
merge.cljs$core$IFn$_invoke$arity$2 = merge__2;
return merge;
})()
;
/**
* Returns a channel containing the single (collection) result of the
* items taken from the channel conjoined to the supplied
* collection. ch must close before into produces a result.
*/
cljs.core.async.into = (function into(coll,ch){return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
* Returns a channel that will return, at most, n items from ch. After n items
* have been returned, or ch has been closed, the return chanel will close.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.take = (function() {
var take = null;
var take__2 = (function (n,ch){return take.call(null,n,ch,null);
});
var take__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___11559 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11559,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11559,out){
return (function (state_11536){var state_val_11537 = (state_11536[1]);if((state_val_11537 === 1))
{var inst_11513 = 0;var state_11536__$1 = (function (){var statearr_11538 = state_11536;(statearr_11538[7] = inst_11513);
return statearr_11538;
})();var statearr_11539_11560 = state_11536__$1;(statearr_11539_11560[2] = null);
(statearr_11539_11560[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 2))
{var inst_11513 = (state_11536[7]);var inst_11515 = (inst_11513 < n);var state_11536__$1 = state_11536;if(cljs.core.truth_(inst_11515))
{var statearr_11540_11561 = state_11536__$1;(statearr_11540_11561[1] = 4);
} else
{var statearr_11541_11562 = state_11536__$1;(statearr_11541_11562[1] = 5);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 3))
{var inst_11533 = (state_11536[2]);var inst_11534 = cljs.core.async.close_BANG_.call(null,out);var state_11536__$1 = (function (){var statearr_11542 = state_11536;(statearr_11542[8] = inst_11533);
return statearr_11542;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11536__$1,inst_11534);
} else
{if((state_val_11537 === 4))
{var state_11536__$1 = state_11536;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11536__$1,7,ch);
} else
{if((state_val_11537 === 5))
{var state_11536__$1 = state_11536;var statearr_11543_11563 = state_11536__$1;(statearr_11543_11563[2] = null);
(statearr_11543_11563[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 6))
{var inst_11531 = (state_11536[2]);var state_11536__$1 = state_11536;var statearr_11544_11564 = state_11536__$1;(statearr_11544_11564[2] = inst_11531);
(statearr_11544_11564[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 7))
{var inst_11518 = (state_11536[9]);var inst_11518__$1 = (state_11536[2]);var inst_11519 = (inst_11518__$1 == null);var inst_11520 = cljs.core.not.call(null,inst_11519);var state_11536__$1 = (function (){var statearr_11545 = state_11536;(statearr_11545[9] = inst_11518__$1);
return statearr_11545;
})();if(inst_11520)
{var statearr_11546_11565 = state_11536__$1;(statearr_11546_11565[1] = 8);
} else
{var statearr_11547_11566 = state_11536__$1;(statearr_11547_11566[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 8))
{var inst_11518 = (state_11536[9]);var state_11536__$1 = state_11536;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11536__$1,11,out,inst_11518);
} else
{if((state_val_11537 === 9))
{var state_11536__$1 = state_11536;var statearr_11548_11567 = state_11536__$1;(statearr_11548_11567[2] = null);
(statearr_11548_11567[1] = 10);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 10))
{var inst_11528 = (state_11536[2]);var state_11536__$1 = state_11536;var statearr_11549_11568 = state_11536__$1;(statearr_11549_11568[2] = inst_11528);
(statearr_11549_11568[1] = 6);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11537 === 11))
{var inst_11513 = (state_11536[7]);var inst_11523 = (state_11536[2]);var inst_11524 = (inst_11513 + 1);var inst_11513__$1 = inst_11524;var state_11536__$1 = (function (){var statearr_11550 = state_11536;(statearr_11550[10] = inst_11523);
(statearr_11550[7] = inst_11513__$1);
return statearr_11550;
})();var statearr_11551_11569 = state_11536__$1;(statearr_11551_11569[2] = null);
(statearr_11551_11569[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11559,out))
;return ((function (switch__7040__auto__,c__7111__auto___11559,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11555 = [null,null,null,null,null,null,null,null,null,null,null];(statearr_11555[0] = state_machine__7041__auto__);
(statearr_11555[1] = 1);
return statearr_11555;
});
var state_machine__7041__auto____1 = (function (state_11536){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11536);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11556){if((e11556 instanceof Object))
{var ex__7044__auto__ = e11556;var statearr_11557_11570 = state_11536;(statearr_11557_11570[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11536);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11556;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11571 = state_11536;
state_11536 = G__11571;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11536){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11536);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11559,out))
})();var state__7113__auto__ = (function (){var statearr_11558 = f__7112__auto__.call(null);(statearr_11558[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11559);
return statearr_11558;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11559,out))
);
return out;
});
take = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return take__2.call(this,n,ch);
case 3:
return take__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
take.cljs$core$IFn$_invoke$arity$2 = take__2;
take.cljs$core$IFn$_invoke$arity$3 = take__3;
return take;
})()
;
/**
* Returns a channel that will contain values from ch. Consecutive duplicate
* values will be dropped.
* 
* The output channel is unbuffered by default, unless buf-or-n is given.
*/
cljs.core.async.unique = (function() {
var unique = null;
var unique__1 = (function (ch){return unique.call(null,ch,null);
});
var unique__2 = (function (ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___11668 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11668,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11668,out){
return (function (state_11643){var state_val_11644 = (state_11643[1]);if((state_val_11644 === 1))
{var inst_11620 = null;var state_11643__$1 = (function (){var statearr_11645 = state_11643;(statearr_11645[7] = inst_11620);
return statearr_11645;
})();var statearr_11646_11669 = state_11643__$1;(statearr_11646_11669[2] = null);
(statearr_11646_11669[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 2))
{var state_11643__$1 = state_11643;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11643__$1,4,ch);
} else
{if((state_val_11644 === 3))
{var inst_11640 = (state_11643[2]);var inst_11641 = cljs.core.async.close_BANG_.call(null,out);var state_11643__$1 = (function (){var statearr_11647 = state_11643;(statearr_11647[8] = inst_11640);
return statearr_11647;
})();return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11643__$1,inst_11641);
} else
{if((state_val_11644 === 4))
{var inst_11623 = (state_11643[9]);var inst_11623__$1 = (state_11643[2]);var inst_11624 = (inst_11623__$1 == null);var inst_11625 = cljs.core.not.call(null,inst_11624);var state_11643__$1 = (function (){var statearr_11648 = state_11643;(statearr_11648[9] = inst_11623__$1);
return statearr_11648;
})();if(inst_11625)
{var statearr_11649_11670 = state_11643__$1;(statearr_11649_11670[1] = 5);
} else
{var statearr_11650_11671 = state_11643__$1;(statearr_11650_11671[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 5))
{var inst_11620 = (state_11643[7]);var inst_11623 = (state_11643[9]);var inst_11627 = cljs.core._EQ_.call(null,inst_11623,inst_11620);var state_11643__$1 = state_11643;if(inst_11627)
{var statearr_11651_11672 = state_11643__$1;(statearr_11651_11672[1] = 8);
} else
{var statearr_11652_11673 = state_11643__$1;(statearr_11652_11673[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 6))
{var state_11643__$1 = state_11643;var statearr_11654_11674 = state_11643__$1;(statearr_11654_11674[2] = null);
(statearr_11654_11674[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 7))
{var inst_11638 = (state_11643[2]);var state_11643__$1 = state_11643;var statearr_11655_11675 = state_11643__$1;(statearr_11655_11675[2] = inst_11638);
(statearr_11655_11675[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 8))
{var inst_11620 = (state_11643[7]);var tmp11653 = inst_11620;var inst_11620__$1 = tmp11653;var state_11643__$1 = (function (){var statearr_11656 = state_11643;(statearr_11656[7] = inst_11620__$1);
return statearr_11656;
})();var statearr_11657_11676 = state_11643__$1;(statearr_11657_11676[2] = null);
(statearr_11657_11676[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 9))
{var inst_11623 = (state_11643[9]);var state_11643__$1 = state_11643;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11643__$1,11,out,inst_11623);
} else
{if((state_val_11644 === 10))
{var inst_11635 = (state_11643[2]);var state_11643__$1 = state_11643;var statearr_11658_11677 = state_11643__$1;(statearr_11658_11677[2] = inst_11635);
(statearr_11658_11677[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11644 === 11))
{var inst_11623 = (state_11643[9]);var inst_11632 = (state_11643[2]);var inst_11620 = inst_11623;var state_11643__$1 = (function (){var statearr_11659 = state_11643;(statearr_11659[7] = inst_11620);
(statearr_11659[10] = inst_11632);
return statearr_11659;
})();var statearr_11660_11678 = state_11643__$1;(statearr_11660_11678[2] = null);
(statearr_11660_11678[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11668,out))
;return ((function (switch__7040__auto__,c__7111__auto___11668,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11664 = [null,null,null,null,null,null,null,null,null,null,null];(statearr_11664[0] = state_machine__7041__auto__);
(statearr_11664[1] = 1);
return statearr_11664;
});
var state_machine__7041__auto____1 = (function (state_11643){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11643);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11665){if((e11665 instanceof Object))
{var ex__7044__auto__ = e11665;var statearr_11666_11679 = state_11643;(statearr_11666_11679[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11643);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11665;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11680 = state_11643;
state_11643 = G__11680;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11643){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11643);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11668,out))
})();var state__7113__auto__ = (function (){var statearr_11667 = f__7112__auto__.call(null);(statearr_11667[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11668);
return statearr_11667;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11668,out))
);
return out;
});
unique = function(ch,buf_or_n){
switch(arguments.length){
case 1:
return unique__1.call(this,ch);
case 2:
return unique__2.call(this,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
unique.cljs$core$IFn$_invoke$arity$1 = unique__1;
unique.cljs$core$IFn$_invoke$arity$2 = unique__2;
return unique;
})()
;
/**
* Returns a channel that will contain vectors of n items taken from ch. The
* final vector in the return channel may be smaller than n if ch closed before
* the vector could be completely filled.
* 
* The output channel is unbuffered by default, unless buf-or-n is given
*/
cljs.core.async.partition = (function() {
var partition = null;
var partition__2 = (function (n,ch){return partition.call(null,n,ch,null);
});
var partition__3 = (function (n,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___11815 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11815,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11815,out){
return (function (state_11785){var state_val_11786 = (state_11785[1]);if((state_val_11786 === 1))
{var inst_11748 = (new Array(n));var inst_11749 = inst_11748;var inst_11750 = 0;var state_11785__$1 = (function (){var statearr_11787 = state_11785;(statearr_11787[7] = inst_11749);
(statearr_11787[8] = inst_11750);
return statearr_11787;
})();var statearr_11788_11816 = state_11785__$1;(statearr_11788_11816[2] = null);
(statearr_11788_11816[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 2))
{var state_11785__$1 = state_11785;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11785__$1,4,ch);
} else
{if((state_val_11786 === 3))
{var inst_11783 = (state_11785[2]);var state_11785__$1 = state_11785;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11785__$1,inst_11783);
} else
{if((state_val_11786 === 4))
{var inst_11753 = (state_11785[9]);var inst_11753__$1 = (state_11785[2]);var inst_11754 = (inst_11753__$1 == null);var inst_11755 = cljs.core.not.call(null,inst_11754);var state_11785__$1 = (function (){var statearr_11789 = state_11785;(statearr_11789[9] = inst_11753__$1);
return statearr_11789;
})();if(inst_11755)
{var statearr_11790_11817 = state_11785__$1;(statearr_11790_11817[1] = 5);
} else
{var statearr_11791_11818 = state_11785__$1;(statearr_11791_11818[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 5))
{var inst_11749 = (state_11785[7]);var inst_11758 = (state_11785[10]);var inst_11753 = (state_11785[9]);var inst_11750 = (state_11785[8]);var inst_11757 = (inst_11749[inst_11750] = inst_11753);var inst_11758__$1 = (inst_11750 + 1);var inst_11759 = (inst_11758__$1 < n);var state_11785__$1 = (function (){var statearr_11792 = state_11785;(statearr_11792[10] = inst_11758__$1);
(statearr_11792[11] = inst_11757);
return statearr_11792;
})();if(cljs.core.truth_(inst_11759))
{var statearr_11793_11819 = state_11785__$1;(statearr_11793_11819[1] = 8);
} else
{var statearr_11794_11820 = state_11785__$1;(statearr_11794_11820[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 6))
{var inst_11750 = (state_11785[8]);var inst_11771 = (inst_11750 > 0);var state_11785__$1 = state_11785;if(cljs.core.truth_(inst_11771))
{var statearr_11796_11821 = state_11785__$1;(statearr_11796_11821[1] = 12);
} else
{var statearr_11797_11822 = state_11785__$1;(statearr_11797_11822[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 7))
{var inst_11781 = (state_11785[2]);var state_11785__$1 = state_11785;var statearr_11798_11823 = state_11785__$1;(statearr_11798_11823[2] = inst_11781);
(statearr_11798_11823[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 8))
{var inst_11749 = (state_11785[7]);var inst_11758 = (state_11785[10]);var tmp11795 = inst_11749;var inst_11749__$1 = tmp11795;var inst_11750 = inst_11758;var state_11785__$1 = (function (){var statearr_11799 = state_11785;(statearr_11799[7] = inst_11749__$1);
(statearr_11799[8] = inst_11750);
return statearr_11799;
})();var statearr_11800_11824 = state_11785__$1;(statearr_11800_11824[2] = null);
(statearr_11800_11824[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 9))
{var inst_11749 = (state_11785[7]);var inst_11763 = cljs.core.vec.call(null,inst_11749);var state_11785__$1 = state_11785;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11785__$1,11,out,inst_11763);
} else
{if((state_val_11786 === 10))
{var inst_11769 = (state_11785[2]);var state_11785__$1 = state_11785;var statearr_11801_11825 = state_11785__$1;(statearr_11801_11825[2] = inst_11769);
(statearr_11801_11825[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 11))
{var inst_11765 = (state_11785[2]);var inst_11766 = (new Array(n));var inst_11749 = inst_11766;var inst_11750 = 0;var state_11785__$1 = (function (){var statearr_11802 = state_11785;(statearr_11802[7] = inst_11749);
(statearr_11802[12] = inst_11765);
(statearr_11802[8] = inst_11750);
return statearr_11802;
})();var statearr_11803_11826 = state_11785__$1;(statearr_11803_11826[2] = null);
(statearr_11803_11826[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 12))
{var inst_11749 = (state_11785[7]);var inst_11773 = cljs.core.vec.call(null,inst_11749);var state_11785__$1 = state_11785;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11785__$1,15,out,inst_11773);
} else
{if((state_val_11786 === 13))
{var state_11785__$1 = state_11785;var statearr_11804_11827 = state_11785__$1;(statearr_11804_11827[2] = null);
(statearr_11804_11827[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 14))
{var inst_11778 = (state_11785[2]);var inst_11779 = cljs.core.async.close_BANG_.call(null,out);var state_11785__$1 = (function (){var statearr_11805 = state_11785;(statearr_11805[13] = inst_11778);
return statearr_11805;
})();var statearr_11806_11828 = state_11785__$1;(statearr_11806_11828[2] = inst_11779);
(statearr_11806_11828[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11786 === 15))
{var inst_11775 = (state_11785[2]);var state_11785__$1 = state_11785;var statearr_11807_11829 = state_11785__$1;(statearr_11807_11829[2] = inst_11775);
(statearr_11807_11829[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11815,out))
;return ((function (switch__7040__auto__,c__7111__auto___11815,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11811 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11811[0] = state_machine__7041__auto__);
(statearr_11811[1] = 1);
return statearr_11811;
});
var state_machine__7041__auto____1 = (function (state_11785){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11785);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11812){if((e11812 instanceof Object))
{var ex__7044__auto__ = e11812;var statearr_11813_11830 = state_11785;(statearr_11813_11830[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11785);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11812;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11831 = state_11785;
state_11785 = G__11831;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11785){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11785);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11815,out))
})();var state__7113__auto__ = (function (){var statearr_11814 = f__7112__auto__.call(null);(statearr_11814[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11815);
return statearr_11814;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11815,out))
);
return out;
});
partition = function(n,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition__2.call(this,n,ch);
case 3:
return partition__3.call(this,n,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition.cljs$core$IFn$_invoke$arity$2 = partition__2;
partition.cljs$core$IFn$_invoke$arity$3 = partition__3;
return partition;
})()
;
/**
* Returns a channel that will contain vectors of items taken from ch. New
* vectors will be created whenever (f itm) returns a value that differs from
* the previous item's (f itm).
* 
* The output channel is unbuffered, unless buf-or-n is given
*/
cljs.core.async.partition_by = (function() {
var partition_by = null;
var partition_by__2 = (function (f,ch){return partition_by.call(null,f,ch,null);
});
var partition_by__3 = (function (f,ch,buf_or_n){var out = cljs.core.async.chan.call(null,buf_or_n);var c__7111__auto___11974 = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__7111__auto___11974,out){
return (function (){var f__7112__auto__ = (function (){var switch__7040__auto__ = ((function (c__7111__auto___11974,out){
return (function (state_11944){var state_val_11945 = (state_11944[1]);if((state_val_11945 === 1))
{var inst_11903 = [];var inst_11904 = inst_11903;var inst_11905 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538);var state_11944__$1 = (function (){var statearr_11946 = state_11944;(statearr_11946[7] = inst_11904);
(statearr_11946[8] = inst_11905);
return statearr_11946;
})();var statearr_11947_11975 = state_11944__$1;(statearr_11947_11975[2] = null);
(statearr_11947_11975[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 2))
{var state_11944__$1 = state_11944;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_11944__$1,4,ch);
} else
{if((state_val_11945 === 3))
{var inst_11942 = (state_11944[2]);var state_11944__$1 = state_11944;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_11944__$1,inst_11942);
} else
{if((state_val_11945 === 4))
{var inst_11908 = (state_11944[9]);var inst_11908__$1 = (state_11944[2]);var inst_11909 = (inst_11908__$1 == null);var inst_11910 = cljs.core.not.call(null,inst_11909);var state_11944__$1 = (function (){var statearr_11948 = state_11944;(statearr_11948[9] = inst_11908__$1);
return statearr_11948;
})();if(inst_11910)
{var statearr_11949_11976 = state_11944__$1;(statearr_11949_11976[1] = 5);
} else
{var statearr_11950_11977 = state_11944__$1;(statearr_11950_11977[1] = 6);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 5))
{var inst_11905 = (state_11944[8]);var inst_11912 = (state_11944[10]);var inst_11908 = (state_11944[9]);var inst_11912__$1 = f.call(null,inst_11908);var inst_11913 = cljs.core._EQ_.call(null,inst_11912__$1,inst_11905);var inst_11914 = cljs.core.keyword_identical_QMARK_.call(null,inst_11905,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",4382193538));var inst_11915 = (inst_11913) || (inst_11914);var state_11944__$1 = (function (){var statearr_11951 = state_11944;(statearr_11951[10] = inst_11912__$1);
return statearr_11951;
})();if(cljs.core.truth_(inst_11915))
{var statearr_11952_11978 = state_11944__$1;(statearr_11952_11978[1] = 8);
} else
{var statearr_11953_11979 = state_11944__$1;(statearr_11953_11979[1] = 9);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 6))
{var inst_11904 = (state_11944[7]);var inst_11929 = inst_11904.length;var inst_11930 = (inst_11929 > 0);var state_11944__$1 = state_11944;if(cljs.core.truth_(inst_11930))
{var statearr_11955_11980 = state_11944__$1;(statearr_11955_11980[1] = 12);
} else
{var statearr_11956_11981 = state_11944__$1;(statearr_11956_11981[1] = 13);
}
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 7))
{var inst_11940 = (state_11944[2]);var state_11944__$1 = state_11944;var statearr_11957_11982 = state_11944__$1;(statearr_11957_11982[2] = inst_11940);
(statearr_11957_11982[1] = 3);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 8))
{var inst_11904 = (state_11944[7]);var inst_11912 = (state_11944[10]);var inst_11908 = (state_11944[9]);var inst_11917 = inst_11904.push(inst_11908);var tmp11954 = inst_11904;var inst_11904__$1 = tmp11954;var inst_11905 = inst_11912;var state_11944__$1 = (function (){var statearr_11958 = state_11944;(statearr_11958[7] = inst_11904__$1);
(statearr_11958[8] = inst_11905);
(statearr_11958[11] = inst_11917);
return statearr_11958;
})();var statearr_11959_11983 = state_11944__$1;(statearr_11959_11983[2] = null);
(statearr_11959_11983[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 9))
{var inst_11904 = (state_11944[7]);var inst_11920 = cljs.core.vec.call(null,inst_11904);var state_11944__$1 = state_11944;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11944__$1,11,out,inst_11920);
} else
{if((state_val_11945 === 10))
{var inst_11927 = (state_11944[2]);var state_11944__$1 = state_11944;var statearr_11960_11984 = state_11944__$1;(statearr_11960_11984[2] = inst_11927);
(statearr_11960_11984[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 11))
{var inst_11912 = (state_11944[10]);var inst_11908 = (state_11944[9]);var inst_11922 = (state_11944[2]);var inst_11923 = [];var inst_11924 = inst_11923.push(inst_11908);var inst_11904 = inst_11923;var inst_11905 = inst_11912;var state_11944__$1 = (function (){var statearr_11961 = state_11944;(statearr_11961[7] = inst_11904);
(statearr_11961[8] = inst_11905);
(statearr_11961[12] = inst_11922);
(statearr_11961[13] = inst_11924);
return statearr_11961;
})();var statearr_11962_11985 = state_11944__$1;(statearr_11962_11985[2] = null);
(statearr_11962_11985[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 12))
{var inst_11904 = (state_11944[7]);var inst_11932 = cljs.core.vec.call(null,inst_11904);var state_11944__$1 = state_11944;return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_11944__$1,15,out,inst_11932);
} else
{if((state_val_11945 === 13))
{var state_11944__$1 = state_11944;var statearr_11963_11986 = state_11944__$1;(statearr_11963_11986[2] = null);
(statearr_11963_11986[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 14))
{var inst_11937 = (state_11944[2]);var inst_11938 = cljs.core.async.close_BANG_.call(null,out);var state_11944__$1 = (function (){var statearr_11964 = state_11944;(statearr_11964[14] = inst_11937);
return statearr_11964;
})();var statearr_11965_11987 = state_11944__$1;(statearr_11965_11987[2] = inst_11938);
(statearr_11965_11987[1] = 7);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_11945 === 15))
{var inst_11934 = (state_11944[2]);var state_11944__$1 = state_11944;var statearr_11966_11988 = state_11944__$1;(statearr_11966_11988[2] = inst_11934);
(statearr_11966_11988[1] = 14);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__7111__auto___11974,out))
;return ((function (switch__7040__auto__,c__7111__auto___11974,out){
return (function() {
var state_machine__7041__auto__ = null;
var state_machine__7041__auto____0 = (function (){var statearr_11970 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];(statearr_11970[0] = state_machine__7041__auto__);
(statearr_11970[1] = 1);
return statearr_11970;
});
var state_machine__7041__auto____1 = (function (state_11944){while(true){
var ret_value__7042__auto__ = (function (){try{while(true){
var result__7043__auto__ = switch__7040__auto__.call(null,state_11944);if(cljs.core.keyword_identical_QMARK_.call(null,result__7043__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__7043__auto__;
}
break;
}
}catch (e11971){if((e11971 instanceof Object))
{var ex__7044__auto__ = e11971;var statearr_11972_11989 = state_11944;(statearr_11972_11989[5] = ex__7044__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_11944);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e11971;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__7042__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__11990 = state_11944;
state_11944 = G__11990;
continue;
}
} else
{return ret_value__7042__auto__;
}
break;
}
});
state_machine__7041__auto__ = function(state_11944){
switch(arguments.length){
case 0:
return state_machine__7041__auto____0.call(this);
case 1:
return state_machine__7041__auto____1.call(this,state_11944);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__7041__auto____0;
state_machine__7041__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__7041__auto____1;
return state_machine__7041__auto__;
})()
;})(switch__7040__auto__,c__7111__auto___11974,out))
})();var state__7113__auto__ = (function (){var statearr_11973 = f__7112__auto__.call(null);(statearr_11973[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__7111__auto___11974);
return statearr_11973;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__7113__auto__);
});})(c__7111__auto___11974,out))
);
return out;
});
partition_by = function(f,ch,buf_or_n){
switch(arguments.length){
case 2:
return partition_by__2.call(this,f,ch);
case 3:
return partition_by__3.call(this,f,ch,buf_or_n);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
partition_by.cljs$core$IFn$_invoke$arity$2 = partition_by__2;
partition_by.cljs$core$IFn$_invoke$arity$3 = partition_by__3;
return partition_by;
})()
;

//# sourceMappingURL=async.js.map