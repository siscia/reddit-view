// Compiled by ClojureScript 0.0-2234
goog.provide('om_tut.core');
goog.require('cljs.core');
goog.require('cljs.core.async');
goog.require('jayq.core');
goog.require('jayq.core');
goog.require('cljs.core.async');
goog.require('om.dom');
goog.require('om.dom');
goog.require('om.core');
goog.require('om.core');
cljs.core.enable_console_print_BANG_.call(null);
om_tut.core.app_state = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"posts","posts",1120759621),new cljs.core.PersistentVector(null, 18, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"http://redtuba.com/"], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"url","url",1014020321),"https://www.reddit.com/"], null)], null)], null));
om_tut.core.global_chanel = cljs.core.async.chan.call(null);
om_tut.core.action = cljs.core.async.pub.call(null,om_tut.core.global_chanel,(function (p1__9384_SHARP_){return new cljs.core.Keyword(null,"action","action",3885920680).cljs$core$IFn$_invoke$arity$1(p1__9384_SHARP_);
}));
om_tut.core.contacts_view = (function contacts_view(app,owner){if(typeof om_tut.core.t9413 !== 'undefined')
{} else
{
/**
* @constructor
*/
om_tut.core.t9413 = (function (owner,app,contacts_view,meta9414){
this.owner = owner;
this.app = app;
this.contacts_view = contacts_view;
this.meta9414 = meta9414;
this.cljs$lang$protocol_mask$partition1$ = 0;
this.cljs$lang$protocol_mask$partition0$ = 393216;
})
om_tut.core.t9413.cljs$lang$type = true;
om_tut.core.t9413.cljs$lang$ctorStr = "om-tut.core/t9413";
om_tut.core.t9413.cljs$lang$ctorPrWriter = (function (this__4105__auto__,writer__4106__auto__,opt__4107__auto__){return cljs.core._write.call(null,writer__4106__auto__,"om-tut.core/t9413");
});
om_tut.core.t9413.prototype.om$core$IRenderState$ = true;
om_tut.core.t9413.prototype.om$core$IRenderState$render_state$arity$2 = (function (this$,p__9416){var self__ = this;
var map__9417 = p__9416;var map__9417__$1 = ((cljs.core.seq_QMARK_.call(null,map__9417))?cljs.core.apply.call(null,cljs.core.hash_map,map__9417):map__9417);var action = cljs.core.get.call(null,map__9417__$1,new cljs.core.Keyword(null,"action","action",3885920680));var this$__$1 = this;return React.DOM.div({"className": "pure-g"},React.DOM.div({"id": "left", "className": "pure-u-1-12"},null),React.DOM.div({"className": "pure-u-20-24", "tabIndex": 0, "id": "test"},React.DOM.iframe({"sanddox": "allow-forms allow-scripts", "id": "frame", "src": new cljs.core.Keyword(null,"url","url",1014020321).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,new cljs.core.Keyword(null,"posts","posts",1120759621).cljs$core$IFn$_invoke$arity$1(self__.app)))},null),React.DOM.button({"onClick": ((function (this$__$1,map__9417,map__9417__$1,action){
return (function (_){return cljs.core.async.put_BANG_.call(null,action,new cljs.core.Keyword(null,"uno","uno",1014020200));
});})(this$__$1,map__9417,map__9417__$1,action))
},"Next")),React.DOM.div({"id": "right", "className": "pure-u-1-12"},null));
});
om_tut.core.t9413.prototype.om$core$IWillMount$ = true;
om_tut.core.t9413.prototype.om$core$IWillMount$will_mount$arity$1 = (function (_){var self__ = this;
var ___$1 = this;var action = om.core.get_state.call(null,self__.owner,new cljs.core.Keyword(null,"action","action",3885920680));var c__6346__auto__ = cljs.core.async.chan.call(null,1);cljs.core.async.impl.dispatch.run.call(null,((function (c__6346__auto__,action,___$1){
return (function (){var f__6347__auto__ = (function (){var switch__6331__auto__ = ((function (c__6346__auto__,action,___$1){
return (function (state_9428){var state_val_9429 = (state_9428[1]);if((state_val_9429 === 4))
{var inst_9420 = (state_9428[2]);var inst_9421 = cljs.core.println.call(null,inst_9420);var inst_9422 = (function (){var action__$1 = inst_9420;return ((function (action__$1,inst_9420,inst_9421,state_val_9429,c__6346__auto__,action,___$1){
return (function (p){return cljs.core.rest.call(null,p);
});
;})(action__$1,inst_9420,inst_9421,state_val_9429,c__6346__auto__,action,___$1))
})();var inst_9423 = om.core.transact_BANG_.call(null,self__.app,new cljs.core.Keyword(null,"posts","posts",1120759621),inst_9422);var state_9428__$1 = (function (){var statearr_9430 = state_9428;(statearr_9430[7] = inst_9423);
(statearr_9430[8] = inst_9421);
return statearr_9430;
})();var statearr_9431_9441 = state_9428__$1;(statearr_9431_9441[2] = null);
(statearr_9431_9441[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if((state_val_9429 === 3))
{var inst_9426 = (state_9428[2]);var state_9428__$1 = state_9428;return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_9428__$1,inst_9426);
} else
{if((state_val_9429 === 2))
{var state_9428__$1 = state_9428;return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_9428__$1,4,action);
} else
{if((state_val_9429 === 1))
{var state_9428__$1 = state_9428;var statearr_9432_9442 = state_9428__$1;(statearr_9432_9442[2] = null);
(statearr_9432_9442[1] = 2);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{return null;
}
}
}
}
});})(c__6346__auto__,action,___$1))
;return ((function (switch__6331__auto__,c__6346__auto__,action,___$1){
return (function() {
var state_machine__6332__auto__ = null;
var state_machine__6332__auto____0 = (function (){var statearr_9436 = [null,null,null,null,null,null,null,null,null];(statearr_9436[0] = state_machine__6332__auto__);
(statearr_9436[1] = 1);
return statearr_9436;
});
var state_machine__6332__auto____1 = (function (state_9428){while(true){
var ret_value__6333__auto__ = (function (){try{while(true){
var result__6334__auto__ = switch__6331__auto__.call(null,state_9428);if(cljs.core.keyword_identical_QMARK_.call(null,result__6334__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
continue;
}
} else
{return result__6334__auto__;
}
break;
}
}catch (e9437){if((e9437 instanceof Object))
{var ex__6335__auto__ = e9437;var statearr_9438_9443 = state_9428;(statearr_9438_9443[5] = ex__6335__auto__);
cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_9428);
return new cljs.core.Keyword(null,"recur","recur",1122293407);
} else
{if(new cljs.core.Keyword(null,"else","else",1017020587))
{throw e9437;
} else
{return null;
}
}
}})();if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__6333__auto__,new cljs.core.Keyword(null,"recur","recur",1122293407)))
{{
var G__9444 = state_9428;
state_9428 = G__9444;
continue;
}
} else
{return ret_value__6333__auto__;
}
break;
}
});
state_machine__6332__auto__ = function(state_9428){
switch(arguments.length){
case 0:
return state_machine__6332__auto____0.call(this);
case 1:
return state_machine__6332__auto____1.call(this,state_9428);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$0 = state_machine__6332__auto____0;
state_machine__6332__auto__.cljs$core$IFn$_invoke$arity$1 = state_machine__6332__auto____1;
return state_machine__6332__auto__;
})()
;})(switch__6331__auto__,c__6346__auto__,action,___$1))
})();var state__6348__auto__ = (function (){var statearr_9439 = f__6347__auto__.call(null);(statearr_9439[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__6346__auto__);
return statearr_9439;
})();return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__6348__auto__);
});})(c__6346__auto__,action,___$1))
);
return c__6346__auto__;
});
om_tut.core.t9413.prototype.om$core$IInitState$ = true;
om_tut.core.t9413.prototype.om$core$IInitState$init_state$arity$1 = (function (_){var self__ = this;
var ___$1 = this;var action = cljs.core.async.chan.call(null);jayq.core.ajax.call(null,"http://www.reddit.com/hot.json",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"success","success",3441701749),((function (action,___$1){
return (function (data){var data__$1 = new cljs.core.Keyword(null,"children","children",2673430897).cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"data","data",1016980252).cljs$core$IFn$_invoke$arity$1(cljs.core.js__GT_clj.call(null,data,new cljs.core.Keyword(null,"keywordize-keys","keywordize-keys",4191781672),true)));var urls = cljs.core.map.call(null,((function (data__$1,action,___$1){
return (function (d){return cljs.core.select_keys.call(null,new cljs.core.Keyword(null,"data","data",1016980252).cljs$core$IFn$_invoke$arity$1(d),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"title","title",1124275658),new cljs.core.Keyword(null,"url","url",1014020321)], null));
});})(data__$1,action,___$1))
,data__$1);om.core.transact_BANG_.call(null,self__.app,new cljs.core.Keyword(null,"posts","posts",1120759621),((function (data__$1,urls,action,___$1){
return (function (___$2){return urls;
});})(data__$1,urls,action,___$1))
);
return cljs.core.println.call(null,self__.app);
});})(action,___$1))
,new cljs.core.Keyword(null,"dataType","dataType",2802975094),"json"], null));
jayq.core.bind.call(null,jayq.core.$.call(null,new cljs.core.Keyword(null,"body","body",1016933652)),new cljs.core.Keyword(null,"keydown","keydown",4493897459),((function (action,___$1){
return (function (e){var G__9440 = e.keyCode;switch (G__9440) {
case 39:
return cljs.core.async.put_BANG_.call(null,action,new cljs.core.Keyword(null,"next","next",1017282149));

break;
case 37:
return cljs.core.async.put_BANG_.call(null,action,new cljs.core.Keyword(null,"back","back",1016920153));

break;
default:
throw (new Error(("No matching clause: "+cljs.core.str.cljs$core$IFn$_invoke$arity$1(e.keyCode))));

}
});})(action,___$1))
);
return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"action","action",3885920680),action], null);
});
om_tut.core.t9413.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_9415){var self__ = this;
var _9415__$1 = this;return self__.meta9414;
});
om_tut.core.t9413.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_9415,meta9414__$1){var self__ = this;
var _9415__$1 = this;return (new om_tut.core.t9413(self__.owner,self__.app,self__.contacts_view,meta9414__$1));
});
om_tut.core.__GT_t9413 = (function __GT_t9413(owner__$1,app__$1,contacts_view__$1,meta9414){return (new om_tut.core.t9413(owner__$1,app__$1,contacts_view__$1,meta9414));
});
}
return (new om_tut.core.t9413(owner,app,contacts_view,null));
});
om.core.root.call(null,om_tut.core.contacts_view,om_tut.core.app_state,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"target","target",4427965699),document.getElementById("container")], null));

//# sourceMappingURL=core.js.map